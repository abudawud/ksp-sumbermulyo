<?php
function format_rupiah($nilai)
{
    return number_format($nilai, 0, ',', '.');
}

function NoUrut($Nomor)
{
    $Panjang = strlen($Nomor);
    if ($Panjang == 1) {
        $nol = "00";
    } else if ($Panjang == 2) {
        $nol = "0";
    } else if ($Panjang >= 3) {
        $nol = "";
    }
    return $nol . "" . $Nomor;
}
