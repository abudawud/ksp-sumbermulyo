<?php
date_default_timezone_set('Asia/Jakarta');
error_reporting(0);
include 'db.php';
include 'fungsi.php';
include 'myLib.php';

session_start();
if ($_SESSION["session_log"] == 1 && !empty($_SESSION["session_nama"])) {
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Koperasi Simpan Pinjam - Sumber Mulyo</title>

        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
        <!-- daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- Bootstrap4 Duallistbox -->
        <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
        <!-- BS Stepper -->
        <link rel="stylesheet" href="plugins/bs-stepper/css/bs-stepper.min.css">
        <!-- dropzonejs -->
        <link rel="stylesheet" href="plugins/dropzone/min/dropzone.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">

        <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">

        <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

        <!-- Added -->
        <link rel="stylesheet" href="dist/css/myCss.css">

        <script src="plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Select2 -->
        <script src="plugins/select2/js/select2.full.min.js"></script>
        <!-- Bootstrap4 Duallistbox -->
        <script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
        <!-- InputMask -->
        <script src="plugins/moment/moment.min.js"></script>
        <script src="plugins/inputmask/jquery.inputmask.min.js"></script>
        <!-- date-range-picker -->
        <script src="plugins/daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap color picker -->
        <script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- Bootstrap Switch -->
        <script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
        <!-- BS-Stepper -->
        <script src="plugins/bs-stepper/js/bs-stepper.min.js"></script>
        <!-- dropzonejs -->
        <script src="plugins/dropzone/min/dropzone.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/adminlte.min.js"></script>

        <script src="plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        
        <script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </li>

                </ul>
            </nav>
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="index3.html" class="brand-link">
                    <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">Sumber Mulyo</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block"><?= $_SESSION["session_nama"] ?></a>
                        </div>
                    </div>
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column nav-legacy nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- <li class="nav-item">
                                <a href="?pages=home" class="nav-link">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li> -->
                            <?php
                            function get_menu_tree($parent_id)
                            {
                                $menu = "";
                                $querymenu = mysql_query("SELECT
                                    a.*, c.id_menu as sub 
                                FROM
                                    tb_menu a
                                    LEFT JOIN tb_karyawan b ON b.menu_akses LIKE concat( '%[', a.id_menu, ']%' ) 
                                    LEFT JOIN tb_menu c ON a.id_menu = c.id_parent AND c.na = 'N'
                                WHERE
                                    b.id_karyawan = '1' 
                                    AND a.na = 'N' 
                                    AND a.id_parent = '$parent_id'
                                GROUP BY 
                                    a.id_menu
                                ORDER BY
                                    a.urut ASC");
                                while ($row = mysql_fetch_array($querymenu)) {
                                    if (!empty($row['sub'])) {
                                        $arrowright = '<i class="right fas fa-angle-left"></i>';
                                    } else {
                                        $arrowright = '';
                                    }
                                    $menu .= '<li class="nav-item">';
                                    $menu .= '<a href="' . $row['url'] . '" class="nav-link">';
                                    $menu .= '<i class="nav-icon ' . $row['icon'] . '"></i>';
                                    $menu .= '<p>' . $row['title'] . $arrowright . '</p>';
                                    $menu .= '</a>';
                                    if (!empty($row['sub'])) {
                                        $menu .= '<ul class="nav nav-treeview">' . get_menu_tree($row['id_menu']) . '</ul>';
                                    }
                                    $menu .= '</li>';
                                }

                                return $menu;
                            }

                            echo get_menu_tree(0);
                            ?>
                            <li class="nav-item">
                                <a href="?page=logout" class="nav-link">
                                    <i class="nav-icon fas fa-sign-out-alt text-danger"></i>
                                    <p class="text">Logout</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>

            <div class="content-wrapper">
                <section class="content-header">

                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- !!!!!!!!!!!!!!!!!!!!!!! disini kontennya !!!!!!!!!!!!!!!!!!!!! -->
                    <?php

                    include 'content.php';
                    ?>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b>Version</b> 1.1.0
                </div>
                <strong>Copyright &copy; 2021 <a href="#">Sumber Mulyo</a></strong>
            </footer>

        </div>
        <script>
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip();
                
                $('.select2').select2();

                //Initialize Select2 Elements
                $('.select2bs4').select2({
                    theme: 'bootstrap4'
                });

                $('.tanggal').datetimepicker({
                    timepicker: false,
                    format: 'YYYY-MM-DD'
                });

            });
        </script>
    </body>

    </html>
<?php
} else {
    include 'login.php';
}
