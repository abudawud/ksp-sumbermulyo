# Credential GitLab Repository (Username dan Password)
#### Jika diminta username dan password untuk push/pull
```
username : atokkecenk@gmail.com
pass/access token : glpat-q7nyyQx5aWiXEdpZNUMX
```

# Php Version
Php 5.6

# Create File (db.php)
#### Default Koneksi
```
<?php
$host = "localhost";
$user = "";
$password = "";
$database = "databasename";

$konek_db = mysql_connect($host, $user, $password);
$find_db = mysql_select_db($database);
if (!$konek_db) {
    die('Gagal Koneksi: ' . mysql_error());
    exit;
}
?>
```

# Deploy Manual Project Via Cpanel
1. Login Cpanel Mentari Karya Gemilang
2. Files -> Git Version Control
![Semantic description of image](https://mentarikaryagemilang.co.id/2022-01-14_093858.png "Git Version Control")
3. Pilih repository **ksp-sumbermulyo**
![Semantic description of image](https://mentarikaryagemilang.co.id/3.png "Repo")
4. Pilih Manage
5. Pilih Tab or Deploy
6. Update from remote
7. Deploy HEAD Commit
![Semantic description of image](https://mentarikaryagemilang.co.id/2022-01-14_092611.png "Pull or Deploy")
8. Selesai

# File Ignore
1. db.php

#### Kunjungi dev project di : https://sumbermulyo.mentarikaryagemilang.co.id/dev/
#### Folder deploy cpanel ada di ***public_html/sumbermulyo/dev/***
