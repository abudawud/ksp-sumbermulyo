<?php
$msg = '';
if (isset($_POST['simpan'])) {
    // $kd_trx      = $_POST['kd_trx'];
    $no_bukti       = $_POST['no_bukti'];
    // $kode_transaksi = $_POST['kode_transaksi'];
    $debet          = $_POST['ak_debet'];
    $kredit         = $_POST['ak_kredit'];
    $tgl            = $_POST['tanggal'];
    $jumlah         = $_POST['jumlah'];
    $jml            = str_replace(['Rp. ', '.'], '', $jumlah);
    $uraian         = $_POST['uraian'];
    $jns_identitas  = $_POST['jns_identitas'];
    $identitas         = $_POST['identitas'];
    $date           = date('y-m-d H:i:s');

    $id_srcmax = mysql_fetch_array(mysql_query("SELECT MAX( src_id_jurnal )+ 1 AS id FROM `tb_jurnal`"));
    $idmax = ($id_srcmax['id'] == '' || $id_srcmax['id'] == null) ? 1 : $id_srcmax['id'];
    for ($i = 1; $i <= 2; $i++) {
        if ($i == 1) {
            $sql = mysql_fetch_array(mysql_query("SELECT * FROM v_rekening WHERE kd_rek = '$debet'"));
            $d = $sql['idrek4'];
            $k = 0;
            $idrek1 = $sql['idrek1'];
            $kdrek1 = $sql['kdrek1'];
            $idrek2 = $sql['idrek2'];
            $kdrek2 = $sql['kdrek2'];
            $idrek3 = $sql['idrek3'];
            $kdrek3 = $sql['kdrek3'];
            $idrek4 = $sql['idrek4'];
            $kdrek4 = $sql['kdrek4'];
            $namarek1 = $sql['namarek1'];
            $namarek2 = $sql['namarek2'];
            $namarek3 = $sql['namarek3'];
            $namarek4 = $sql['namarek4'];
        } else {
            $sql = mysql_fetch_array(mysql_query("SELECT * FROM v_rekening WHERE kd_rek = '$kredit'"));
            $d = 0;
            $k = $sql['idrek4'];
            $idrek1 = $sql['idrek1'];
            $kdrek1 = $sql['kdrek1'];
            $idrek2 = $sql['idrek2'];
            $kdrek2 = $sql['kdrek2'];
            $idrek3 = $sql['idrek3'];
            $kdrek3 = $sql['kdrek3'];
            $idrek4 = $sql['idrek4'];
            $kdrek4 = $sql['kdrek4'];
            $namarek1 = $sql['namarek1'];
            $namarek2 = $sql['namarek2'];
            $namarek3 = $sql['namarek3'];
            $namarek4 = $sql['namarek4'];
        }

        $ins = mysql_query("INSERT INTO tb_jurnal (no_bukti, 
                            kode_transaksi, 
                            tanggal, 
                            debet, 
                            kredit, 
                            uraian, 
                            keterangan, 
                            jumlah, 
                            id_identitas, 
                            id_divisi, 
                            tgl_input, 
                            user_input, 
                            st_input, 
                            jenis_identitas, 
                            idrek1, 
                            kdrek1, 
                            idrek2, 
                            kdrek2, 
                            idrek3, 
                            kdrek3, 
                            idrek4, 
                            kdrek4, 
                            namarek1, 
                            namarek2, 
                            namarek3, 
                            namarek4, 
                            src_id_jurnal, 
                            id_jurnalumum, 
                            asal
                        ) VALUES ('$no_bukti',
                            '',
                            '$tgl',
                            '$d',
                            '$k',
                            '$uraian',
                            '',
                            '$jml',
                            '$identitas',
                            '1',
                            '$date',
                            '$_SESSION[session_user]',
                            '1',
                            '$jns_identitas',
                            '$idrek1',
                            '$kdrek1',
                            '$idrek2',
                            '$kdrek2',
                            '$idrek3',
                            '$kdrek3',
                            '$idrek4',
                            '$kdrek4',
                            '$namarek1',
                            '$namarek2',
                            '$namarek3',
                            '$namarek4',
                            '$idmax',
                            '0',
                            '0'
                        )
        ") or die(mysql_error());
    }

    if ($ins) {
        $msg = 1;
    } else {
        $msg = 0;
    }
}
?>
<style>
    .jumlah {
        background-color: #ffc0c0 !important;
        color: #000 !important;
    }
</style>
<div class="container-fluid">
    <?php
    if (isset($msg) && $msg !== '') {
        if ($msg == 1) {
            echo '<div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Berhasil simpan
                    </div>';
        } elseif ($msg == 0) {
            echo '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Gagal simpan !!
                    </div>';
        }
    }
    ?>
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Jurnal Umum</h3>

            <div class="card-tools">
                <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
            </div>
        </div>
        <div class="card-body">
            <form class="form-horizontal" action="" method="post">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Nomor Bukti</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="no_bukti" placeholder="Nomor Bukti">
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Kode Transaksi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="kode_transaksi" placeholder="Kode Transaksi">
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Akun Debet</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="ak_debet" style="width: 100%;">
                                <option value="">Pilih Rekening</option>
                                <?php
                                $sql = mysql_query("SELECT * FROM v_rekening");
                                while ($rows = mysql_fetch_array($sql)) {
                                ?>
                                    <option value="<?= $rows['kd_rek'] ?>"><?= $rows['kd_rek'] . ' - ' . $rows['namarek4'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Akun Kredit</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="ak_kredit" style="width: 100%;">
                                <option value="">Pilih Rekening</option>
                                <?php
                                $sql = mysql_query("SELECT * FROM v_rekening");
                                while ($rows = mysql_fetch_array($sql)) {
                                ?>
                                    <option value="<?= $rows['kd_rek'] ?>"><?= $rows['kd_rek'] . ' - ' . $rows['namarek4'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Tanggal</label>
                        <div class="col-sm-8">
                            <div class="input-group date" data-target-input="nearest">
                                <input type="text" class="form-control datepicker-input tanggal" name="tanggal" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off">
                                <div class="input-group-append" data-target="#datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Jumlah</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="rupiah" oninput="jumlahInput()" name="jumlah" placeholder="Rp.">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Uraian</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="3" name="uraian" placeholder="Uraian"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Jenis Identitas</label>
                        <div class="col-sm-8">
                            <!-- <select class="form-control select2" name="jns_identitas" style="width: 100%;">
                                <option value="">Pilih Jenis Identitas</option>
                            </select> -->
                            <select id="jns_identitas" name="jns_identitas" onchange="ajaxkota(this.value)" class="input-large form-control select2 select2-accessiblee">
                                <option value="" selected>- Silahkan Pilih -</option>
                                <?php
                                $q = mysql_query("SELECT jenis_identitas FROM v_jenis_identitas GROUP BY jenis_identitas"); //choose the city from indonesia only
                                while ($row1 = mysql_fetch_array($q)) {
                                    $row_jenis = str_replace(" ", "_", $row1[jenis_identitas]);
                                    echo "<option value=$row_jenis>$row1[jenis_identitas]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Identitas</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="identitas" id="identitas" style="width: 100%;">
                                <option value="">Pilih Identitas</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right"></label>
                        <div class="col-sm-8">
                            <button type="submit" class="btn btn-success" name="simpan"><i class="fas fa-check-circle mr-2"></i>Simpan</button>
                            <button type="reset" class="btn btn-danger"><i class="fas fa-ban mr-2"></i>Batal</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var rupiah = document.getElementById('rupiah');

    function jumlahInput() {
        rupiah.classList.add("jumlah");
    }

    rupiah.addEventListener('keyup', function(e) {
        rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    // $(document).ready(function() {
    //     $('[name="jns_identitas"]').change(function() {
    //         $('[name="identitas"]').load('<?= 'myLib.php?load_page=identitas&jenis=' ?>' + $(this).val());
    //     });
    // });
</script>
<script type="text/javascript">
    function ajaxkota(id) {
        ajaxku = buatajax();
        var url = "<?php echo './modul/jurnal/identitas.php' ?>";
        url = url + "?q=" + id;
        url = url + "&sid=" + Math.random();
        ajaxku.onreadystatechange = stateChanged;
        ajaxku.open("GET", url, true);
        ajaxku.send(null);
    }

    function buatajax() {
        if (window.XMLHttpRequest) {
            return new XMLHttpRequest();
        }
        if (window.ActiveXObject) {
            return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }

    function stateChanged() {
        var data;
        if (ajaxku.readyState == 4) {
            data = ajaxku.responseText;
            if (data.length >= 0) {
                document.getElementById("identitas").innerHTML = data
            } else {
                document.getElementById("identitas").value = "<option selected>Pilih Identitas</option>";
            }
        }
    }
</script>