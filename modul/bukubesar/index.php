<?php
if (isset($_POST['filter'])) {
    if (($_POST['tgl1'] == "") && ($_POST['tgl2']) == "") {
        $tgl1 = "2008-01-01";
        $tgl2 = date('Y-m-d', strtotime('+30 days', strtotime(date('Y-m-d'))));
    } else {
        $tgl1 = $_POST['tgl1'];
        $tgl2 = $_POST['tgl2'];
        $tglawal = date('Y-m-d', strtotime('-1 days', strtotime($tgl1)));
    }
    $jenis          = $_POST['jns_identitas'];
    $identitas      = $_POST['identitas'];
    $uraian         = $_POST['uraian'];
    $nobukti        = $_POST['nobukti'];
    $kodetransaksi  = $_POST['kodetransaksi'];
    $rekening       = $_POST['rekening'];

    if ($jenis == '') {
        $carijenis = "";
    } else {
        $data_jenis = str_replace("_", " ", $jenis);
        $carijenis = "AND j.jenis_identitas='$data_jenis' ";
    }

    if ($identitas == '' || $identitas == 'Pilih Identitas') {
        $cariidentitas = "";
    } else {
        $cariidentitas = "AND j.id_identitas='$identitas' ";
    }

    if ($uraian == '') {
        $cariuraian = "";
    } else {
        $cariuraian = "AND j.uraian like '%$uraian%' ";
    }

    if ($nobukti == '') {
        $carinobukti = "";
    } else {
        $carinobukti = "AND j.no_bukti like '%$nobukti%' ";
    }

    if ($kodetransaksi == '') {
        $carikodetransaksi = "";
    } else {
        $carikodetransaksi = "AND j.kode_transaksi like '%$kodetransaksi%' ";
    }

    if ($rekening == '') {
        $carirekening = "";
    } else {
        $carirekening = " AND j.idrek4='$rekening' ";
    }
}

// $identitas      = $_POST['identitas'];
// $identitas      = $_SESSION['identitas'];
// echo $_POST['identitas'];
?>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">BUKU BESAR BANTU</h3>

            <div class="card-tools">
                <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
            </div>
        </div>
        <div class="card-body">
            <form class="form-horizontal" method="post" name="cari" action="<?php echo $_SERVER['REQUEST_URI'] ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal Awal</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <!-- <input type="date" name="tgl1" id="tgl1" class="form-control"> -->
                                    <input type="text" class="form-control datepicker-input tanggal" name="tgl1" id="tgl1" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off" value="<?php echo ($_POST['tgl1'] != "") ? $_POST['tgl1'] : ""; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal Akhir</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <!-- <input type="date" name="tgl2" id="tgl2" class="form-control"> -->
                                    <input type="text" class="form-control datepicker-input tanggal" name="tgl2" id="tgl2" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off" value="<?php echo ($_POST['tgl2'] != "") ? $_POST['tgl2'] : ""; ?>">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Rekening</label>
                                <select name="rekening" id="rekening" class="form-control select2bs4">
                                    <option value="">Pilih Rekening</option>
                                    <?php
                                    $qrek = mysql_query("SELECT * FROM v_rekening");
                                    while ($rowrek = mysql_fetch_array($qrek)) {
                                        if ($rowrek['idrek4'] == $_POST['rekening'])
                                            echo "<option value='" . $rowrek['idrek4'] . "' selected>" . $rowrek['kd_rek'] . " - " . $rowrek['namarek4'] . "</option>";
                                        else
                                            echo "<option value='" . $rowrek['idrek4'] . "'>" . $rowrek['kd_rek'] . " - " . $rowrek['namarek4'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jenis Identitas</label>
                                <select name="jns_identitas" id="jns_identitas" class="form-control select2bs4" onchange="ajaxkota(this.value)">
                                    <option value="" selected>- Silahkan Pilih -</option>
                                    <?php
                                    $q = mysql_query("SELECT jenis_identitas FROM v_jenis_identitas GROUP BY jenis_identitas"); //choose the city from indonesia only
                                    while ($row1 = mysql_fetch_array($q)) {
                                        // $row_jenis = str_replace(" ", "_", $row1['jenis_identitas']);
                                        if ($row1['jenis_identitas'] == $_POST['jns_identitas'])
                                            echo '<option value="' . $row1['jenis_identitas'] . '" selected>' . $row1['jenis_identitas'] . '</option>';
                                        else
                                            echo '<option value="' . $row1['jenis_identitas'] . '">' . $row1['jenis_identitas'] . '</option>';
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Identitas</label>
                                <select name="identitas" id="identitas" class="form-control select2bs4">
                                    <option value="">Pilih Identitas</option>
                                    <?php
                                    $sql_supp = mysql_query("SELECT * FROM v_identitas WHERE jenis_identitas = '$_POST[jns_identitas]'");
                                    while ($r_supp = mysql_fetch_array($sql_supp)) {
                                        if ($r_supp['kode'] == $_POST['identitas'])
                                            echo "<option value='$r_supp[kode]' selected>$r_supp[nama]</option>";
                                        else
                                            echo "<option value='$r_supp[kode]'>$r_supp[nama]</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>No. Bukti</label>
                                <input type="text" name="no_bukti" id="no_bukti" class="form-control">
                            </div>
                        </div>
                        <!-- <div class="col-md-3">
                            <div class="form-group">
                                <label>Kode Transaksi</label>
                                <input type="text" name="kode_transaksi" id="kode_transaksi" class="form-control">
                            </div>
                        </div> -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Uraian</label>
                                <input type="text" name="uraian" id="uraian" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="text-center">
                        <button type="submit" name="filter" class="btn btn-md btn-primary btn-flat"><i class="fa fa-search"></i> TAMPILKAN</button>
                        <button type="button" class="btn btn-md btn-success btn-flat export"><i class="fa fa-file-excel"></i> EXCEL</button>
                    </div>
                </div>
        </div>
        <?php if ($_POST) { ?>
            <div class="table-responsive">
                <table class="table table-bordered table-hovered table-sm" id="table">
                    <thead>
                        <tr class="bg-blue">
                            <th width="5%" class="text-center">NO</th>
                            <th width="20%">KODE REKENING</th>
                            <th>NAMA REKENING</th>
                            <th width="10%">DEBET</th>
                            <th width="10%">KREDIT</th>
                            <th width="10%">JUMLAH</th>
                            <th width="5%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $bukubesar = "SELECT
                                            `j`.`id_divisi` AS `id_divisi`,
                                            `k`.`idrek4`    AS `idrek4`,
                                            `k`.`kd_rek`    AS `kd_rek`,
                                            `k`.`namarek4`  AS `namarek4`,
                                            SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) AS `debet`,
                                            SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)) AS `kredit`,
                                            (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0))) AS `saldo`
                                            FROM (`tb_jurnal` `j`
                                            JOIN `v_rekening` `k`
                                                    ON (((`k`.`idrek4` = `j`.`debet`)
                                                            OR (`k`.`idrek4` = `j`.`kredit`))))
                                            WHERE (`j`.`tanggal` BETWEEN '$tgl1' AND '$tgl2') $carirekening $carijenis $cariidentitas $cariuraian $carinobukti $carikodetransaksi
                                            GROUP BY `k`.`idrek4`
                                            ORDER BY `k`.`kd_rek`
                                            ";
                        // echo $bukubesar;
                        $no = 0;
                        $totaldebet = 0;
                        $totalkredit = 0;
                        $totalsaldo = 0;
                        $qbukubesar = mysql_query($bukubesar);
                        while ($key = mysql_fetch_object($qbukubesar)) {
                            $no++;
                            $totaldebet = $totaldebet + $key->debet;
                            $totalkredit = $totalkredit + $key->kredit;
                            $totalsaldo = $totalsaldo + $key->saldo;
                        ?>
                            <tr data-widget="expandable-table" aria-expanded="false" onclick="getrinci('<?php echo $key->idrek4; ?>')">
                                <td><?= "<div style='text-align:center;'>" . $no . "<div>"; ?></td>
                                <td><?= $key->kd_rek; ?></td>
                                <td><?= $key->namarek4; ?></td>
                                <td><?= "<div style='text-align:right;'>" . number_format($key->debet) . "<div>"; ?></td>
                                <td><?= "<div style='text-align:right;'>" . number_format($key->kredit) . "<div>"; ?></td>
                                <td><?= "<div style='text-align:right;'>" . number_format($key->saldo) . "<div>"; ?></td>
                                <td class="text-center">
                                    <div class="btn-group" style="width: 100%;">
                                        <button type="button" class="btn btn-xs btn-outline-success exportRinci" data-id="<?php echo $key->idrek4; ?>"><i class="fa fa-file-excel"></i> </button>
                                        <button type="button" class="btn btn-xs btn-outline-danger pdfRinci" data-id="<?php echo $key->idrek4; ?>"><i class="fa fa-file-pdf"></i> </button>
                                    </div>
                                </td>
                            </tr>
                            <tr class="expandable-body" id="<?php echo $key->idrek4; ?>"></tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">
                                <div style='text-align:center;'>TOTAL<div>
                            </th>
                            <th><?= "<div style='text-align:right;'>" . number_format($totaldebet) . "<div>" ?></th>
                            <th><?= "<div style='text-align:right;'>" . number_format($totalkredit) . "<div>" ?></th>
                            <th><?= "<div style='text-align:right;'>" . number_format($totalsaldo) . "<div>" ?></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        <?php } ?>
        </form>
    </div>
</div>
<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    /* function load_bast(id) {
        $.ajax({
            type: "POST",
            url: "./modul/bukubesar/edit.php",
            data: {
                id: id,
            },
            success: function(response) {
                $(".modal-content").html(response);
            }
        });
    } */

    function getrinci(idrek4) {
        // console.log(kd_rek);
        $.ajax({
            type: 'post',
            url: './modul/bukubesar/detail.php',
            data: {
                idrek4: idrek4,
                tgl1: $('#tgl1').val(),
                tgl2: $('#tgl2').val(),
                jenis_identitas: $('#jns_identitas').val(),
                id_identitas: $('#identitas').val(),
                rekening: $('#rekening').val(),
                no_bukti: $('#no_bukti').val(),
                kode_transaksi: $('#kode_transaksi').val(),
                uraian: $('#uraian').val(),
            },
            beforeSend: function() {
                $('.loader').show();
                // document.body.style.filter = "blur(1px)";
            },
            success: function(response) {
                // document.body.style.filter = "blur(0px)";
                $('.loader').hide();
                // We get the element having id of display_info and put the response inside it
                $('#table').find('tr[id="' + idrek4 + '"]').html(response);
                // console.log('sdjskhfksdhfks');
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('[name="jns_identitas"]').change(function() {
            $('[name="identitas"]').load('<?= 'myLib.php?load_page=identitas&jenis=' ?>' + $(this).val());
        });
    });

    function ajaxkota(id) {
        ajaxku = buatajax();
        var url = "<?php echo './modul/jurnal/identitas.php' ?>";
        url = url + "?q=" + id;
        url = url + "&sid=" + Math.random();
        ajaxku.onreadystatechange = stateChanged;
        ajaxku.open("GET", url, true);
        ajaxku.send(null);
    }

    function buatajax() {
        if (window.XMLHttpRequest) {
            return new XMLHttpRequest();
        }
        if (window.ActiveXObject) {
            return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }

    function stateChanged() {
        var data;
        if (ajaxku.readyState == 4) {
            data = ajaxku.responseText;
            if (data.length >= 0) {
                document.getElementById("identitas").innerHTML = data
            } else {
                document.getElementById("identitas").value = "<option value='' selected>Pilih Identitas</option>";
            }
        }
    }
</script>