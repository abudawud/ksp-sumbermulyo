<?php
error_reporting(0);
include '../../db.php';
$idrek4 = $_POST['idrek4'];
if (($_POST['tgl1'] == "") && ($_POST['tgl2']) == "") {
  $tgl1 = "2008-01-01";
  $tgl2 = date('Y-m-d', strtotime('+30 days', strtotime(date('Y-m-d'))));
} else {
  $tgl1 = $_POST['tgl1'];
  $tgl2 = $_POST['tgl2'];
  $tglawal = date('Y-m-d', strtotime('-1 days', strtotime($tgl1)));
}

$jenis          = $_POST['jenis_identitas'];
$identitas      = $_POST['id_identitas'];
$uraian         = $_POST['uraian'];
$nobukti        = $_POST['nobukti'];
$kodetransaksi  = $_POST['kodetransaksi'];
$rekening       = $_POST['rekening'];

if ($jenis == '') {
  $carijenis = "";
} else {
  $data_jenis = str_replace("_", " ", $jenis);
  $carijenis = "AND t1.jenis_identitas='$data_jenis' ";
}

if ($identitas == '' || $identitas == 'Pilih Identitas') {
  $cariidentitas = "";
} else {
  $cariidentitas = "AND t1.id_identitas='$identitas' ";
}

if ($uraian == '') {
  $cariuraian = "";
} else {
  $cariuraian = "AND t1.uraian like '%$uraian%' ";
}

if ($nobukti == '') {
  $carinobukti = "";
} else {
  $carinobukti = "AND t1.no_bukti like '%$nobukti%' ";
}

if ($kodetransaksi == '') {
  $carikodetransaksi = "";
} else {
  $carikodetransaksi = "AND t1.kode_transaksi like '%$kodetransaksi%' ";
}

if ($rekening == '') {
  $carirekening = "";
} else {
  $carirekening = " AND t1.idrek4='$rekening' ";
}
?>
<td colspan="7">
  <table class="table table-bordered table-hover table-striped table-sm" id="table" style="width: 100%;margin: 0;">
    <thead>
      <tr class="bg-gray">
        <th width="10%">TANGGAL</th>
        <th width="10%">NO. BUKTI</th>
        <th width="15%">IDENTITAS</th>
        <th>URAIAN</th>
        <th width="10%">DEBET</th>
        <th width="10%">KREDIT</th>
        <th width="10%">SALDO</th>
        <th width="5%" class="text-center"><i class="fas fa-cog"></i></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php
        $saldoawal = "SELECT
                        t1.`id_divisi`,
                        CONCAT(t1.kdrek1,t1.kdrek2,t1.kdrek3,t1.kdrek4) AS kd_rek,
                        t1.`idrek4`,
                        t1.`namarek4`,
                        SUM(IF(t1.`debet` != 0, t1.jumlah, 0)) AS debet,
                        SUM(IF(t1.`kredit` != 0, t1.jumlah, 0)) AS kredit,
                        SUM(IF(t1.`debet` != 0, t1.jumlah, 0)) - SUM(IF(t1.`kredit` != 0, t1.jumlah, 0)) AS saldoawal
                      FROM
                        `tb_jurnal` t1
                        WHERE t1.idrek4 = '$idrek4' AND t1.`tanggal` BETWEEN '2015-01-01' AND '$tglawal' $carijenis $cariidentitas $cariuraian $carinobukti $carikodetransaksi
                      GROUP BY
                        t1.`idrek4`
                      ORDER BY
                        CONCAT(t1.kdrek1,t1.kdrek2,t1.kdrek3,t1.kdrek4)";
        $querysa = mysql_query($saldoawal);
        $rowsa = mysql_fetch_array($querysa);
        ?>
        <td class="text-right" colspan="4">Saldo Awal:</td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"><?= (empty($rowsa['saldoawal'])) ? 0 : number_format($rowsa['saldoawal']); ?></td>
        <td class="text-right"></td>
      </tr>
      <?php
      $totaldebet = 0;
      $totalkredit = 0;
      $qrinci = mysql_query("SELECT
				`id_jurnal`,
				`no_bukti`,
				`kode_transaksi`,
				`tanggal`,
				`uraian`,
				`keterangan`,
				`jumlah`,
				`id_identitas`,
				`tgl_input`,
				`user_input`,
				`st_input`,
				`src_id_jurnal`,
				`id_jurnalumum`,
				`asal`,
				(IF(`debet` != 0, jumlah, 0)) AS debet,
				(IF(`kredit` != 0, jumlah, 0)) AS kredit
			FROM
				`tb_jurnal` t1 
			WHERE t1.idrek4 = '$idrek4' AND t1.tanggal BETWEEN '$tgl1' AND '$tgl2' $carijenis $cariidentitas $cariuraian $carinobukti $carikodetransaksi
			ORDER BY
			`tanggal`, id_jurnal");


      while ($value = mysql_fetch_object($qrinci)) {
      ?>
        <tr>
          <td><?= $value->tanggal; ?></td>
          <td><?= $value->no_bukti; ?></td>
          <td></td>
          <td><?= $value->uraian; ?></td>
          <td class="text-right"><?= number_format($value->debet); ?></td>
          <td class="text-right"><?= number_format($value->kredit); ?></td>
          <td class="text-right"><?= number_format($saldoawal = ($rowsa['saldoawal'] + $value->debet) - $value->kredit); ?></td>
          <!-- <td><button type="button" class="btn btn-xs btn-outline-primary" data-id="<?php //echo $key->idrek4; 
                                                                                          ?>" style="width: 100%;"><i class="fa fa-edit"></i> Edit</button></td> -->
          <!-- <td><a href="./modul/bukubesar/edit.php?id=<?php //echo $value->src_id_jurnal; 
                                                          ?>" data-toggle="modal" data-target="#myModal">Edit</a></td> -->
          <td><a href="javascript:;" class="btn btn-xs btn-outline-primary" data-toggle="modal" data-target="#modal-lg" style="width: 100%;" onclick="javascript:load_edit('<?= $value->id_jurnal; ?>','<?= (!empty($value->src_id_jurnal)) ? $value->src_id_jurnal : $value->id_jurnalumum; ?>','<?= $value->asal ?>')"><i class="fa fa-edit"></i> Rinci</a></td>
        </tr>
      <?php
        $totaldebet = $totaldebet + $value->debet;
        $totalkredit = $totalkredit + $value->kredit;
      } ?>
    </tbody>
    <tfoot>
      <tr class="bg-gray">
        <th class="text-right" colspan="4">Total (Hasil Pencarian):</th>
        <th class="text-right"><?= number_format($totaldebet); ?></th>
        <th class="text-right"><?= number_format($totalkredit); ?></th>
        <th class="text-right"><?= number_format($totalnya = $totaldebet - $totalkredit); ?></th>
        <th></th>
      </tr>
    </tfoot>
  </table>
</td>

<script>
  function load_edit(id_jurnal, src_id_jurnal, asal) {
    $.ajax({
      type: "POST",
      url: "./modul/bukubesar/edit.php",
      data: {
        id_jurnal: id_jurnal,
        src_id_jurnal: src_id_jurnal,
        asal: asal
      },
      success: function(response) {
        $(".modal-content").html(response);
      }
    });
  }
</script>