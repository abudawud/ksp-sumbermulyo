<?php
error_reporting(0);
include '../../db.php';
$data_jurnal = mysql_fetch_array(mysql_query("SELECT * FROM tb_jurnal WHERE id_jurnal='$_POST[id_jurnal]'"));
?>
<div class="modal-header">
	<h4 class="modal-title"><strong>DETAIL BUKU BESAR</strong></h4>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
	<input type="hidden" name="id_jurnal" value="<?php echo $_GET['id_jurnal']; ?>">
	<div class="box-group">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label>NO BUKTI</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data_jurnal['no_bukti'] ?>" name="no_bukti">
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label>TANGGAL</label>
						<input type="date" class="form-control form-control-sm" value="<?php echo $data_jurnal['tanggal'] ?>" name="tanggal">
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label>URIAIAN</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data_jurnal['uraian'] ?>" name="uraian">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label>JENIS IDENTITAS</label>
						<select id="jid" name="jid" onchange="ajaxkota(this.value)" class="input-large form-control form-control-sm js-example-basic-single">
							<option value="" selected>- Silahkan Pilih -</option>
						</select>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label>IDENTITAS</label>
						<select name="supplier1" id="supplier1" class="input-large form-control form-control-sm js-example-basic-single">
							<option value="">- Silahkan Pilih -</option>
						</select>
					</div>
				</div>
				<input type="hidden" class="form-control form-control-sm" readonly value="<?php echo $data_jurnal['user_input'] ?>">
				<input type="hidden" class="form-control form-control-sm" readonly value="<?php echo $data_jurnal['tgl_input'] ?>">
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label>KETERANGAN</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data_jurnal['keterangan'] ?>" name="keterangan">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="form-group">
						<label>USER INPUT</label>
						<input type="text" class="form-control form-control-sm" readonly value="<?php echo $data_jurnal['user_input'] ?>" name="user_input">
					</div>
				</div>
			</div>
			<br />
			<?php
			if ($_SESSION['login_level'] == 'finance_manager') { ?>
				<table class="table table-bordered table-striped table-hover" width="90%" align="center">
					<tr class="warning">
						<th width="60%">NAMA REKENING</th>
						<th width="20%">DEBET</th>
						<th width="20%">KREDIT</th>
					</tr>
					<tr>
						<td>
							<select name="iddebet" class="form-control form-control-sm js-example-basic-single col-md-12">
								<?php
								$s_debet = mysql_query("SELECT * FROM v_kdrek");
								while ($r_debet = mysql_fetch_array($s_debet)) {
								?>
									<option <?php if ($r_debet['idrek5'] == $data_jurnal['debet']) {
												echo "selected";
											} ?> value="<?php echo $r_debet['idrek5'] ?>"><?php echo $r_debet['kd_rek'] . " - " . $r_debet['namarek5'] ?></option>
								<?php } ?>
							</select>
						</td>
						<td align="right"><input type="number" class="form-control form-control-sm ratakanan" oninput="doMath1()" name="debet" id="debet" style="text-align: right;" value="<?php echo $data_jurnal['jumlah']; ?>"></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<select name="idkredit" class="form-control form-control-sm js-example-basic-single col-md-12">
								<?php
								$s_kredit = mysql_query("SELECT * FROM v_kdrek");
								while ($r_kredit = mysql_fetch_array($s_kredit)) {
								?>
									<option <?php if ($r_kredit['idrek5'] == $data_jurnal['kredit']) {
												echo "selected";
											} ?> value="<?php echo $r_kredit['idrek5'] ?>"><?php echo $r_kredit['kd_rek'] . " - " . $r_kredit['namarek5'] ?></option>
								<?php } ?>
							</select>
						</td>
						<td></td>
						<td align="right"><input type="number" class="form-control form-control-sm ratakanan" oninput="doMath2()" name="kredit" id="kredit" style="text-align: right;" value="<?php echo $data_jurnal['jumlah']; ?>"></td>
					</tr>
				</table>
			<?php } else { ?>
				<table class="table table-bordered table-striped table-hover table-sm small" width="100%" align="center">
					<tr class="warning">
						<th>KODE REKENING</th>
						<th>NAMA REKENING</th>
						<th>DEBET</th>
						<th>KREDIT</th>
					</tr>
					<?php
					$data_debet = mysql_fetch_array(mysql_query("SELECT b.kd_rek ,b.namarek4 FROM tb_jurnal a LEFT JOIN v_rekening b ON a.idrek4=b.idrek4 WHERE a.src_id_jurnal='$_POST[src_id_jurnal]' AND a.debet!=0 GROUP BY a.src_id_jurnal"));
					$data_kredit = mysql_fetch_array(mysql_query("SELECT b.kd_rek ,b.namarek4 FROM tb_jurnal a LEFT JOIN v_rekening b ON a.idrek4=b.idrek4 WHERE a.src_id_jurnal='$_POST[src_id_jurnal]' AND a.kredit!=0 GROUP BY a.src_id_jurnal"));
					?>
					<tr>
						<td><?php echo $data_debet['kd_rek']; ?></td>
						<td><?php echo $data_debet['namarek4']; ?></td>
						<td align="right"><?php echo number_format($data_jurnal['jumlah'], 0, ',', '.'); ?></td>
						<td></td>
					</tr>
					<tr>
						<td><?php echo $data_kredit['kd_rek']; ?></td>
						<td><?php echo $data_kredit['namarek4']; ?></td>
						<td></td>
						<td align="right"><?php echo number_format($data_jurnal['jumlah'], 0, ',', '.'); ?></td>
					</tr>
				</table>
			<?php }
			?>
		</div>
	</div>

	<div class="modal-footer">
		<?php if ($_SESSION['group_level'] == "manager") { ?>
			<button type="submit" name="simpan" class="btn btn-outline-primary"><i class="fa fa-save"></i> Simpan</button>
		<?php } ?>
		<button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal"><i class="fa fa-close"></i>Tutup</button>
	</div>