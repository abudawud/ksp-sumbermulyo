<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=BUKU BESAR.xls");

require('../../config/db.php');

$rekening       = $_GET['kd_rek'];
// $tglmulai       = $_GET['tglmulai'];
$tglmulai        = date('Y-m-d', strtotime('-1 days', strtotime($_GET['tglmulai'])));
$tglakhir       = $_GET['tglakhir'];
$divisi         = $_GET['divisi'];
$jenis          = $_GET['data_jenis'];
$supplier       = $_GET['supplier'];
$uraian         = $_GET['uraian'];
$nobukti        = $_GET['nobukti'];
$kodetransaksi  = $_GET['kodetransaksi'];

if ($divisi == '') {
    $cari = "";
} else {
    $cari = "AND id_divisi='$divisi' ";
}

if ($jenis == '') {
    $carijenis = "";
} else {
    $data_jenis = str_replace("_", " ", $jenis);
    $carijenis = "AND a.jenis_identitas='$data_jenis' ";
}

if ($supplier == '') {
    $carisupp = "";
} else {
    $carisupp = "AND id_identitas='$supplier' ";
}

if ($uraian == '') {
    $cariuraian = "";
} else {
    $cariuraian = "AND uraian like '%$uraian%' ";
}

if ($nobukti == '') {
    $carinobukti = "";
} else {
    $carinobukti = "AND no_bukti like '%$nobukti%' ";
}

if ($kodetransaksi == '') {
    $carikodetransaksi = "";
} else {
    $carikodetransaksi = "AND kode_transaksi like '%$kodetransaksi%' ";
}

if ($rekening == '') {
    $carirekening = "";
} else {
    $carirekening = "AND kd_rek = '$rekening'";
}


$kategori = mysql_query("SELECT
                            kd_rek as kd_rek,
                            idrek5 as idrek5,
                            `namarek5` as namarek5
                        FROM
                            v_kdrek t1
                        where kd_rek = '$rekening'
                ") or die(mysql_error());
$k = mysql_fetch_array($kategori);
?>
<table>
    <thead>
        <tr>
            <th colspan="6"><img src="http://simbca.benihcitraasia.com/simbca/assets/logobca.png" style="height: 300px;"></th>
            <td colspan="4">PT. BENIH CITRA ASIA <br>
                Jl. Akmaludin No.26, PO. BOX 26 Jember 68175 <br>
                Jawa Timur - Indonesia
            </td>
        </tr>
        <tr>
            <th colspan="11" style="background-color: green;">
                <h3>BUKU BESAR</h3>
            </th>
        </tr>
    </thead>
</table>
<br>
<table border="1">
    <thead>
        <tr>
            <td colspan="11" style="background-color: aqua; text-align: left; vertical-align: middle; height: 28px;font-size: 20px;"><strong><?php echo $k[kd_rek] . " - " . $k[namarek5]; ?></strong></td>
        </tr>
        <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>No. Bukti</th>
            <th colspan="2">Identitas</th>
            <th colspan="3" style="width: 600px;">Uraian</th>
            <th>Debet</th>
            <th>Kredit</th>
            <th>Saldo</th>
        </tr>
    </thead>
    <tr>
        <?php
        $saldoawal = "SELECT
                    `k`.`kd_rek`    AS `kd_rek`,
                    (SUM(IF((`k`.`idrek5` = `a`.`debet`),`a`.`jumlah`,0)) - SUM(IF((`k`.`idrek5` = `a`.`kredit`),`a`.`jumlah`,0))) AS `saldo`
                  FROM (`fnc_jurnal` `a`
                     JOIN `v_kdrek` `k`
                       ON (((`k`.`idrek5` = `a`.`debet`)
                             OR (`k`.`idrek5` = `a`.`kredit`))))
                  WHERE (`a`.`tanggal` BETWEEN '2015-01-01'
                         AND '$tglmulai') $cari $carirekening $carijenis $carisupp $cariuraian $carinobukti $carikodetransaksi
                  GROUP BY `k`.`idrek5`
                  ORDER BY `k`.`kd_rek`
                    ";

        // echo $saldoawal;
        $querysa = mysql_query($saldoawal);
        $rowsa = mysql_fetch_array($querysa);
        ?>
        <td style="text-align: right" colspan="8">Saldo Awal : </td>
        <td style="text-align: right"><?php  ?></td>
        <td style="text-align: right"><?php  ?></td>
        <td style="text-align: right"><?php $awal = $rowsa['saldo'];
                                        echo ($awal); ?></td>
    </tr>
    <?php
    $jurnal = mysql_query("SELECT
                            a.`id_jurnal`,
                            a.`no_bukti`,
                            a.`kode_transaksi`,
                            a.`tanggal`,
                            a.`uraian`,
                            a.`jumlah` AS `debet`,
                            0 AS `kredit`,
                            b.nama_supplier
                          FROM
                            `fnc_jurnal` a
                            LEFT JOIN `view_identitas` b
                              ON a.`id_identitas` = b.`kode_supplier`
                          WHERE a.tanggal BETWEEN '$_GET[tglmulai]'
                            AND '$tglakhir'
                            AND a.`debet` = '$k[idrek5]' $cari $carijenis $carisupp $cariuraian $carinobukti $carikodetransaksi
                          UNION
                          ALL
                          SELECT
                            a.`id_jurnal`,
                            a.`no_bukti`,
                            a.`kode_transaksi`,
                            a.`tanggal`,
                            a.`uraian`,
                            0 AS `debet`,
                            a.`jumlah` AS `kredit`,
                            b.nama_supplier
                          FROM
                            `fnc_jurnal` a
                            LEFT JOIN `view_identitas` b
                              ON a.`id_identitas` = b.`kode_supplier`
                          WHERE a.tanggal BETWEEN '$_GET[tglmulai]'
                            AND '$tglakhir'
                            AND a.`kredit` = '$k[idrek5]' $cari $carijenis $carisupp $cariuraian $carinobukti $carikodetransaksi
                        ORDER BY `tanggal`, id_jurnal ASC") or die(mysql_error());

    $no = 1;
    $tsaldo = 0;
    $saldo = $awal;
    $tnilai = 0;
    $tterbayar = 0;

    while ($data = mysql_fetch_array($jurnal)) {

        if ($data['debet'] > 0) {
            $tsaldo += $data['debet'];
        } else {
            $tsaldo -= $data['debet'] + $data['kredit'];
        }

        $saldo = ($saldo + $data['debet']) - $data['kredit'];
        echo "
            <tr>
                <td>" . $no . "</td>
                <td>" . $data['tanggal'] . "</td>
                <td>" . $data['no_bukti'] . "</td>
                <td colspan='2'>" . $data['nama_supplier'] . "</td>
                <td colspan='3'>" . $data['uraian'] . "</td>
                <td>" . ($data['debet']) . "</td>
                <td>" . ($data['kredit']) . "</td>
                <td>" . ($saldo) . "</td>
            </tr>";

        $no++;

        $totaldebet = $totaldebet + $data['debet'];
        $totalkredit = $totalkredit + $data['kredit'];
    }
    ?>
    <tr>
        <th class="text-right" colspan="8">Total per Pencarian : </th>
        <th width="10%" style="text-align: right;"><?php echo ($totaldebet); ?></th>
        <th width="10%" style="text-align: right;"><?php echo ($totalkredit); ?></th>
        <th width="10%" style="text-align: right;"><?php $totalnya = $totaldebet - $totalkredit;
                                                    echo ($totalnya); ?></th>
    </tr>
</table>