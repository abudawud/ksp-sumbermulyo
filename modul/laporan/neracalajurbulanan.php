<?php
if (isset($_POST['filter'])) {
    $tahun          = $_POST['tahun'];

    if ($jenis == '') {
        $carijenis = "";
    } else {
        $data_jenis = str_replace("_", " ", $jenis);
        $carijenis = "AND a.jenis_identitas='$data_jenis' ";
    }

    if ($identitas == '') {
        $cariidentitas = "";
    } else {
        $cariidentitas = "AND id_identitas='$identitas' ";
    }

    if ($uraian == '') {
        $cariuraian = "";
    } else {
        $cariuraian = "AND uraian like '%$uraian%' ";
    }

    if ($nobukti == '') {
        $carinobukti = "";
    } else {
        $carinobukti = "AND no_bukti like '%$nobukti%' ";
    }

    if ($kodetransaksi == '') {
        $carikodetransaksi = "";
    } else {
        $carikodetransaksi = "AND kode_transaksi like '%$kodetransaksi%' ";
    }

    if ($rekening == '') {
        $carirekening = "";
    } else {
        $carirekening = " AND idrek4='$rekening' ";
    }
}
?>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Neraca Lajur Bulanan</h3>

            <div class="card-tools">
                <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
            </div>
        </div>
        <div class="card-body">
            <form class="form-horizontal" action="" method="post">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tahun</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <select name="tahun" id="tahun" class="form-control select2bs4">
                                        <option value="">Pilih Tahun</option>
                                        <?php for ($i = date('Y'); $i >= 2015; $i--) {
                                            if ($_POST['tahun'] == $i)
                                                echo "<option value='" . $i . "' selected>" . $i . "</option>";
                                            else
                                                echo "<option value='" . $i . "'>" . $i . "</option>";
                                        } ?>
                                    </select>
                                    <button type="submit" name="filter" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-search"></i> TAMPILKAN</button>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Awal</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl1" id="tgl1" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Akhir</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl2" id="tgl2" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off">

                                </div>
                            </div> 
                    </div>-->
                    </div>
                </div>
                <!-- <div class="box-footer">
                    <div class="text-center">
                        <button type="submit" name="filter" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-search"></i> TAMPILKAN</button>
                    </div>
                </div> -->
        </div>
        <style>
            .kecilkan {
                padding-top: 0px;
                padding-bottom: 0px;
            }
        </style>
        <?php if (!empty($_POST['tahun'])) { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hovered table-bordered table-sm" id="table">
                            <thead>
                                <tr class="bg-blue">
                                    <th class="text-center" width="5%">NO</th>
                                    <th class="text-center" width="10%">KODE</th>
                                    <th class="text-center">NAMA REKENING</th>
                                    <th class="text-center">JANUARI</th>
                                    <th class="text-center">FEBRUARI</th>
                                    <th class="text-center">MARET</th>
                                    <th class="text-center">APRIL</th>
                                    <th class="text-center">MEI</th>
                                    <th class="text-center">JUNI</th>
                                    <th class="text-center">JULI</th>
                                    <th class="text-center">AGUSTUS</th>
                                    <th class="text-center">SEPTEMBER</th>
                                    <th class="text-center">OKTOBER</th>
                                    <th class="text-center">NOVEMBER</th>
                                    <th class="text-center">DESEMBER</th>
                                    <th class="text-center" width="10%">JUMLAH</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $n = 0;
                                $total_saldo11 = 0;
                                $rekening11 = "SELECT
                                            `j`.`id_divisi` AS `id_divisi`,
                                            `k`.`idrek4`    AS `idrek4`,
                                            `k`.`kd_rek`    AS `kd_rek`,
                                            `k`.`namarek4`  AS `namarek4`,
                                            SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) AS `debet`,
                                            SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)) AS `kredit`,
                                            (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0))) AS `saldo`
                                            FROM (`tb_jurnal` `j`
                                            JOIN `v_rekening` `k`
                                                    ON (((`k`.`idrek4` = `j`.`debet`)
                                                            OR (`k`.`idrek4` = `j`.`kredit`))))
                                            WHERE YEAR(`j`.`tanggal`)='$tahun'
                                            GROUP BY `j`.`kdrek1`,`j`.`kdrek2`
                                            ORDER BY `k`.`kd_rek`
                                            ";
                                $no = 0;
                                $totalrekening11 = 0;
                                $qrekening11 = mysql_query($rekening11);
                                while ($key11 = mysql_fetch_object($qrekening11)) {
                                    $n++;
                                ?>
                                    <tr>
                                        <td><?= "<div style='text-align:center;'>" . $n . "<div>"; ?></td>
                                        <td><?= $key11->kd_rek; ?></td>
                                        <td><?= $key11->namarek4; ?></td>
                                        <td>
                                            <?php
                                            echo "<div style='text-align:right;'>" . number_format($key11->saldo) . "<div>";
                                            ?>
                                        </td>
                                    </tr>
                                <?php
                                    $total_saldo11 = $total_saldo11 + $key11->saldo;
                                }
                                $total = $total + $total_saldo11;
                                ?>
                                <tr class="bg-gray">
                                    <th class="text-center" colspan="3">TOTAL:</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th class="text-right">0</th>
                                    <th width="10%" class="text-right"><?= number_format($total_saldo11); ?></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>
        </form>
    </div>
</div>