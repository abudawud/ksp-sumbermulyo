<?php
$aks = isset($_GET['aks']) ? $_GET['aks'] : '';
$ik = isset($_GET['ik']) ? $_GET['ik'] : '';
$whr = '';
$msg = '';
if ($aks == 'upd') {
    $whr = "a.id_karyawan = '$ik'";
}
if (isset($_POST['simpan'])) {
    $user       = $_POST['user'];
    $password   = $_POST['password'];
    $conf_pass  = $_POST['conf_pass'];
    $nama       = $_POST['nama'];
    $alamat     = $_POST['alamat'];
    $jk         = $_POST['jk'];
    $agama      = $_POST['agama'];
    $jabatan    = $_POST['jabatan'];
    $level      = $_POST['level'];
    $email      = $_POST['email'];
    $menu      = $_POST['menu'];
    $reg        = date('y-m-d H:i:s');
    for ($x = 0; $x < count($menu); $x++) {
        if ($x == 0) {
            $menuAkses = "[" . $menu[$x] . "]";
        } else {
            $menuAkses = $menuAkses . ",[" . $menu[$x] . "]";
        }
    }

    if (count($menu) == 0) {
        $msg = 0;
        $text = 'Pilih akses menu dahulu !!';
    } else {
        $allow = array('gif', 'png', 'jpg', 'jpeg', '');
        $namaFile = ($_FILES['file']['name'] !== '') ? $_FILES['file']['name'] : '';
        $namaSementara = ($_FILES['file']['name'] !== '') ? $_FILES['file']['tmp_name'] : '';
        $dirUpload = "dist/img/";
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        if (in_array($ext, $allow)) {
            if ($password == $conf_pass) {
                if ($namaFile == '') {
                    $nmFile = null;
                } else {
                    $nmFile = md5(rand()) . '.' . $ext;
                    move_uploaded_file($namaSementara, $dirUpload . $nmFile);
                }
                $kode = buat_kode('tb_karyawan', 'kd_karyawan', 'IDK', 3);
                $pwd = md5($password);
                $ins = mysql_query("INSERT INTO tb_karyawan (kd_karyawan, 
                                    nama, 
                                    alamat, 
                                    sex, 
                                    agama, 
                                    email, 
                                    username, 
                                    `password`, 
                                    jenis_identitas, 
                                    `level`, 
                                    menu_akses, 
                                    foto,
                                    register, 
                                    last_login
                                ) VALUES ('$kode',
                                    '$nama',
                                    '$alamat',
                                    '$jk',
                                    '$agama',
                                    '$email',
                                    '$user',
                                    '$pwd',
                                    'KARYAWAN',
                                    '$level',
                                    '$menuAkses',
                                    '$nmFile',
                                    '$reg',
                                    '$reg'
                                )
                            ") or die(mysql_error());

                if ($ins) {
                    $msg = 1;
                    $text = 'Berhasil simpan.';
                } else {
                    $msg = 0;
                    $text = 'Gagal simpan data !!';
                }
            } else {
                $msg = 0;
                $text = 'Password tidak vallid !!';
            }
        } else {
            $msg = 0;
            $text = 'Mohon file berupa jpg, jpeg, png, gif.';
        }
        echo "<meta http-equiv='default-style'content='0;url=?page=anggota'> ";
    }
} elseif (isset($_POST['update'])) {
    $nama       = $_POST['nama'];
    $alamat     = $_POST['alamat'];
    $jk         = $_POST['jk'];
    $agama      = $_POST['agama'];
    $level      = $_POST['level'];
    $email      = $_POST['email'];
    $menu      = $_POST['menu'];

    $allow = array('gif', 'png', 'jpg', 'jpeg');
    $namaFile = ($_FILES['file']['name'] !== '') ? $_FILES['file']['name'] : '';
    $namaSementara = ($_FILES['file']['name'] !== '') ? $_FILES['file']['tmp_name'] : '';
    $dirUpload = "dist/img/";
    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
    if ($namaFile !== '') {
        $nmFile = md5(rand()) . '.' . $ext;
        move_uploaded_file($namaSementara, $dirUpload . $nmFile);
        $qFoto = ",foto = '$nmFile'";
    } else {
        $qFoto = null;
    }

    for ($x = 0; $x < count($menu); $x++) {
        if ($x == 0) {
            $menuAkses = "[" . $menu[$x] . "]";
        } else {
            $menuAkses = $menuAkses . ",[" . $menu[$x] . "]";
        }
    }

    $na = isset($_POST['na']) ? 'Y' : 'N';
    $upd = mysql_query("UPDATE tb_karyawan SET
                                    nama = '$nama',
                                    alamat = '$alamat',
                                    sex = '$jk',
                                    agama = '$agama',
                                    email = '$email',
                                    `level` = '$level',
                                    menu_akses = '$menuAkses'
                                    $qFoto
                                WHERE id_karyawan = '$ik'
                            ") or die(mysql_error());
    if ($upd) {
        $msg = 1;
        $text = 'Berhasil update data.';
    } else {
        $msg = 0;
        $text = 'Gagal update data !!';
    }
}
?>
<style>
    .menu>ul {
        margin-left: 20px;
    }

    .wtree li {
        list-style-type: none;
        /* margin: 10px 0 10px 10px; */
        margin: 6px 0 6px 10px;
        position: relative;
    }

    .wtree li:before {
        content: "";
        position: absolute;
        top: -10px;
        left: -20px;
        border-left: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
        width: 20px;
        height: 15px;
    }

    .wtree li:after {
        position: absolute;
        content: "";
        top: 5px;
        left: -20px;
        border-left: 1px solid #ddd;
        border-top: 1px solid #ddd;
        width: 20px;
        height: 100%;
    }

    .wtree li:last-child:after {
        display: none;
    }

    .wtree li span {
        display: block;
        border: 1px solid #ddd;
        /* padding: 10px; */
        padding: 1px 0 0 11px;
        color: #888;
        text-decoration: none;
    }

    .span0 {
        list-style-type: none;
        border: 1px solid #ddd;
        display: block;
        padding: 0 0 0 10px;
        color: #888;
        margin-bottom: 5px;
    }

    li {
        list-style-type: none;
    }

    fieldset.scheduler-border {
        border: 1px solid #17a2b8 !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        border-radius: 5px;
    }

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width: auto;
        padding: 0 10px;
        border-bottom: none;
    }

    .scroll {
        width: 443px;
        height: 565px;
        overflow: auto;
        padding-right: 5px;
    }
</style>
<div class="container-fluid">
    <?php
    if (isset($msg) && $msg !== '') {
        if ($msg == 1) {
            echo '<div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        ' . $text . '
                    </div>';
        } elseif ($msg == 0) {
            echo '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        ' . $text . '
                    </div>';
        }
    }
    ?>
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title"><?= ($aks == 'upd') ? 'Update Karyawan' : 'Tambah Karyawan' ?></h3>

            <div class="card-tools">
                <div class="card-tools">
                    <a href="?page=karyawan-view" class="btn btn-tool" title="Kembali ke view">
                        <i class="fas fa-arrow-left"></i> Kembali
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?php
            if ($aks !== 'upd') {
            ?>
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Login User</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="user">
                                    <span id="icoCekUser" style="font-size: 13px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right"></label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="conf_pass">
                                    <span id="validPwd" style="font-size: 13px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Nama User</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Alamat User</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="3" name="alamat"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Jenis Kelamin</label>
                                <div class="col-sm-8" style="padding-top: 6px;">
                                    <?php
                                    $no = 1;
                                    foreach (get_kelamin('') as $jk) {
                                        echo '
                                        <div class="icheck-primary d-inline mr-5">
                                            <input type="radio" id="jk' . $no . '" name="jk" value="' . $jk['id'] . '" >
                                            <label for="jk' . $no . '">' . $jk['kelamin'] . '
                                            </label>
                                        </div>
                                    ';
                                        $no++;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Agama</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="agama">
                                        <option value="">Pilih</option>
                                        <?php
                                        $no = 1;
                                        foreach (get_agama('result', '', '') as $val) {
                                            echo '<option value="' . $val['id'] . '">' . $no . '. ' . $val['agama'] . '</option>';
                                            $no++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Level User</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="level">
                                        <option value="">Pilih</option>
                                        <?php
                                        $no = 1;
                                        foreach (get_user_level('result', '', '') as $val) {
                                            echo '<option value="' . $val['id'] . '">' . $no . '. ' . $val['level'] . '</option>';
                                            $no++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Foto</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" name="file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right"></label>
                                <div class="col-sm-8">
                                    <button type="submit" class="btn btn-success" name="simpan"><i class="fas fa-check-circle mr-2"></i>Simpan</button>
                                    <button type="reset" class="btn btn-danger"><i class="fas fa-ban mr-2"></i>Batal</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 menu">
                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Akses Menu</legend>
                                <div class="scroll">
                                    <div class="control-group">
                                        <ul style="padding: 0; font-size: 14px;">
                                            <?php
                                            function menu_tree($parent_id)
                                            {
                                                $menu = '';
                                                $sql = mysql_query("SELECT
                                                                    a.*, c.id_menu as sub 
                                                                FROM
                                                                    tb_menu a
                                                                    LEFT JOIN tb_karyawan b ON b.menu_akses LIKE concat( '%[', a.id_menu, ']%' ) 
                                                                    LEFT JOIN tb_menu c ON a.id_menu = c.id_parent AND c.na = 'N'
                                                                WHERE
                                                                    b.id_karyawan = '1' 
                                                                    AND a.na = 'N' 
                                                                    AND a.id_parent = '$parent_id'
                                                                GROUP BY 
                                                                    a.id_menu
                                                                ORDER BY
                                                                    a.urut ASC
                                                                    ");

                                                while ($row = mysql_fetch_array($sql)) {
                                                    $cls = ($parent_id == 0) ? 'class="span0"' : '';
                                                    $menu .= '<li>';
                                                    $menu .= '<span ' . $cls . '><input type="checkbox" name="menu[]" value="' . $row['id_menu'] . '"> ' . $row['title'] . '</span>';
                                                    if (!empty($row['sub'])) {
                                                        $menu .= '<ul class="wtree">' . menu_tree($row['id_menu']) . '</ul>';
                                                    }
                                                    $menu .= '</li>';
                                                }

                                                return $menu;
                                            }

                                            echo menu_tree(0);
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </fieldset>

                        </div>
                    </div>
                </form>
            <?php
            } else {
            ?>
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-7">
                            <!-- <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Login User</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="user" value="get_karyawan('row', 'username', $whr) ?>">
                                    <span id="icoCekUser" style="font-size: 13px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="password" value="get_anggota('row', 'nama', $whr) ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right"></label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" name="conf_pass" value="get_anggota('row', 'nama', $whr) ?>">
                                    <span id="validPwd" style="font-size: 13px;"></span>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Nama User</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama" value="<?= get_karyawan('row', 'nama', $whr) ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Alamat User</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="3" name="alamat"><?= get_karyawan('row', 'alamat', $whr) ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Jenis Kelamin</label>
                                <div class="col-sm-8" style="padding-top: 6px;">
                                    <?php
                                    $no = 1;
                                    foreach (get_kelamin('') as $jk) {
                                        $id_jk = get_karyawan('row', 'id_kelamin', $whr);
                                        $jjk = ($jk['id'] == $id_jk) ? 'checked' : null;
                                        echo '
                                        <div class="icheck-primary d-inline mr-5">
                                            <input type="radio" id="jk' . $no . '" name="jk" value="' . $jk['id'] . '" ' . $jjk . '>
                                            <label for="jk' . $no . '">' . $jk['kelamin'] . '
                                            </label>
                                        </div>
                                    ';
                                        $no++;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Agama</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="agama">
                                        <option value="">Pilih</option>
                                        <?php
                                        $no = 1;
                                        foreach (get_agama('result', '', '') as $val) {
                                            $slc = ($val['id'] == get_karyawan('row', 'id_agama', $whr)) ? 'selected' : null;
                                            echo '<option value="' . $val['id'] . '" ' . $slc . '>' . $no . '. ' . $val['agama'] . '</option>';
                                            $no++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Level User</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="level">
                                        <option value="">Pilih</option>
                                        <?php
                                        $no = 1;
                                        foreach (get_user_level('result', '', '') as $val) {
                                            $slc = ($val['id'] == get_karyawan('row', 'id_lev', $whr)) ? 'selected' : null;
                                            echo '<option value="' . $val['id'] . '" ' . $slc . '>' . $no . '. ' . $val['level'] . '</option>';
                                            $no++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email" value="<?= get_karyawan('row', 'email', $whr) ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Foto</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" name="file">
                                    <?php 
                                    ?>
                                    <?php if (!empty(get_karyawan('row', 'foto', $whr))) { ?>
                                        <img src="<?= './dist/img/' . get_karyawan('row', 'foto', $whr) ?>" class="mt-2" style="border: 1px solid #e6e6e6;" width="180">
                                    <?php } else {
                                        echo '<span class="text-danger mt-2">Tidak ada foto yang diupload !!</span>';
                                    } ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right"></label>
                                <div class="col-sm-8">
                                    <button type="submit" class="btn btn-success" name="update"><i class="fas fa-check-circle mr-2"></i>Simpan</button>
                                    <button type="reset" class="btn btn-danger"><i class="fas fa-ban mr-2"></i>Batal</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 menu">
                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Akses Menu</legend>
                                <div class="scroll">
                                    <div class="control-group">
                                        <ul style="padding: 0; font-size: 14px;">
                                            <?php
                                            function menu_tree($parent_id, $idk)
                                            {
                                                $menu = '';
                                                $sql = mysql_query("SELECT
                                                                    a.*, c.id_menu as sub 
                                                                FROM
                                                                    tb_menu a
                                                                    LEFT JOIN tb_karyawan b ON b.menu_akses LIKE concat( '%[', a.id_menu, ']%' ) 
                                                                    LEFT JOIN tb_menu c ON a.id_menu = c.id_parent AND c.na = 'N'
                                                                WHERE
                                                                    b.id_karyawan = '1' 
                                                                    AND a.na = 'N' 
                                                                    AND a.id_parent = '$parent_id'
                                                                GROUP BY 
                                                                    a.id_menu
                                                                ORDER BY
                                                                    a.urut ASC
                                                                    ");

                                                while ($row = mysql_fetch_array($sql)) {
                                                    $cek = mysql_fetch_array(mysql_query("SELECT
                                                                                            COUNT( nama ) AS hitung,
                                                                                            nama,
                                                                                            menu_akses 
                                                                                        FROM
                                                                                            `tb_karyawan` 
                                                                                        WHERE
                                                                                            id_karyawan = '$idk'
                                                                                            AND menu_akses LIKE '%[$row[id_menu]]%'"));
                                                    $check = ((int)$cek['hitung'] !== 0) ? 'checked' : null;
                                                    $cls = ($parent_id == 0) ? 'class="span0"' : '';
                                                    $menu .= '<li>';
                                                    $menu .= '<span ' . $cls . '><input type="checkbox" name="menu[]" value="' . $row['id_menu'] . '" ' . $check . '> ' . $row['title'] . '</span>';
                                                    if (!empty($row['sub'])) {
                                                        $menu .= '<ul class="wtree">' . menu_tree($row['id_menu'], $idk) . '</ul>';
                                                    }
                                                    $menu .= '</li>';
                                                }

                                                return $menu;
                                            }

                                            echo menu_tree(0, $ik);
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </fieldset>

                        </div>
                    </div>
                </form>
            <?php
            }
            ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('[name="user"]').keyup(function() {
            var str = $(this).val();
            var spasi = $(this).val().indexOf(' ') >= 0;
            if (str !== '') {
                if (spasi) {
                    $('#icoCekUser').html('<span class="text-danger"><i class="fas fa-times mr-1"></i>Tidak boleh ada spasi !!</span>');
                } else {
                    $.post("myLib.php", {
                        post: "post",
                        mode: "cekUsername",
                        user: str
                    }, function(result) {
                        if (result == '0') {
                            $('#icoCekUser').html('<span class="text-success"><i class="fas fa-check mr-1"></i>User tersedia</span>');
                        } else {
                            $('#icoCekUser').html('<span class="text-danger"><i class="fas fa-times mr-1"></i>User sudah ada !!</span>');
                        }
                    });
                }
            } else {
                $('#icoCekUser').html('');
            }
        });

        $('[name="conf_pass"]').keyup(function() {
            var pwd = $('[name="password"]').val();
            var cpwd = $(this).val();
            if (cpwd !== '') {
                if (cpwd == pwd) {
                    $('#validPwd').html('<span class="text-success"><i class="fas fa-check mr-1"></i>Password valid</span>');
                } else {
                    $('#validPwd').html('<span class="text-danger"><i class="fas fa-times mr-1"></i>Password tidak valid</span>');
                }
            } else {
                $('#validPwd').html('');
            }

        });
    });
</script>