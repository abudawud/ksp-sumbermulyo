<?php
$msg = '';
$page = $_GET['page'];
$get = mysql_fetch_array(mysql_query("SELECT * FROM tb_rek_pendaftaran WHERE id = 1")) or die(mysql_error());
if (isset($_POST['update'])) {
    $upd = mysql_query("UPDATE tb_rek_pendaftaran SET nominal = '$_POST[nominal]',
                                                keterangan = '$_POST[keterangan]',
                                                rek_debet = '$_POST[ak_debet]',
                                                rek_kredit = '$_POST[ak_kredit]',
                                                update_by = '$_SESSION[session_user]',
                                                update_at = NOW()
                                            WHERE id = '1'") or die(mysql_error());

    if ($upd) {
        $msg = 1;
    } else {
        $msg = 0;
    }
    echo "<meta http-equiv='refresh'content='1;url=?page=" . $page . "'> ";
}
?>
<div class="container-fluid">
    <div class="col-12">
        <?php
        if (isset($msg) && $msg !== '') {
            if ($msg == 1) {
                echo '<div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Berhasil update data.
                    </div>';
            } elseif ($msg == 0) {
                echo '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Gagal update data !!
                    </div>';
            }
        }
        ?>
        <div class="card card-info">
            <div class="card-header ui-sortable-handle">
                <h3 class="card-title">Rekening Pendaftaran</h3>

                <!-- <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Minimize data">
                        <i class="fas fa-minus"></i>
                    </button>
                    <a href="?page=anggota" class="btn btn-tool" title="Tambah Anggota">
                        <i class="fas fa-plus"></i>
                    </a>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Tutup Data">
                        <i class="fas fa-times"></i>
                    </button>
                </div> -->
            </div>
            <form method="post" action="">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label text-right">Nominal</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="nominal" id="rupiah" value="<?= $get['nominal'] ?>" placeholder="Rp. 0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label text-right">Keterangan</label>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="2" name="keterangan"><?= $get['keterangan'] ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label text-right">Rekening Debet</label>
                        <div class="col-sm-5">
                            <select class="form-control select2" name="ak_debet" style="width: 100%;">
                                <option value="">Pilih Rekening</option>
                                <?php
                                // $deb = get_jurnal_bykd_anggota('row', 'kd_rek', 'a.uraian = "pendaftaran/' . get_anggota("row", "kode", $whr) . '" AND a.kredit = "0"');
                                $sql = mysql_query("SELECT * FROM v_rekening");
                                while ($rows = mysql_fetch_array($sql)) {
                                    $slc = ($rows['idrek4'] == $get['rek_debet']) ? 'selected' : null;
                                ?>
                                    <option value="<?= $rows['idrek4'] ?>" <?= $slc ?>><?= $rows['kd_rek'] . ' - ' . $rows['namarek4'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label text-right">Rekening Kredit</label>
                        <div class="col-sm-5">
                            <select class="form-control select2" name="ak_kredit" style="width: 100%;">
                                <option value="">Pilih Rekening</option>
                                <?php
                                // $kre = get_jurnal_bykd_anggota('row', 'kd_rek', 'a.uraian = "pendaftaran/' . get_anggota("row", "kode", $whr) . '" AND a.debet = "0"');
                                $sql = mysql_query("SELECT * FROM v_rekening");
                                while ($rows = mysql_fetch_array($sql)) {
                                    $slc = ($rows['idrek4'] == $get['rek_kredit']) ? 'selected' : null;
                                ?>
                                    <option value="<?= $rows['idrek4'] ?>" <?= $slc ?>><?= $rows['kd_rek'] . ' - ' . $rows['namarek4'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-right"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-primary" name="update"><i class="fas fa-save mr-2"></i>Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>