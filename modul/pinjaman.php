<?php
if (isset($_POST["simpan"])) {
    $ins = mysql_query("INSERT INTO tb_produkpinjaman (
    kode_produk,
    nama_produk,
    jenis_produk,
    max_pinjaman,
    efektif,
    jasa,
    jml_cicilan,
    metode_hitung,
    idrek_debet,
    idrek_kredit,
    idrek_debet_jasa,
    idrek_kredit_jasa,
    nilai_operasional,
    idrek_debet_provisi,
    idrek_kredit_provisi,
    nilai_provisi,
    idrek_debet_pencairan,
    idrek_kredit_pencairan,
    idrek_debet_denda,
    idrek_kredit_denda,
    nilai_denda,
    idrek_debet_admin,
    idrek_kredit_admin,
    nilai_admin) VALUES (
        '$_POST[kode_produk]',
        '$_POST[nama_produk]',
        '$_POST[jenis_jasa]',
        '$_POST[max_pinjaman]',
        '$_POST[efektif]',
        '$_POST[jasa_produk]',
        '$_POST[jumlah_cicilan]',
        '$_POST[hitung_jasa]',
        '$_POST[rekening_debet_mengangsur]',
        '$_POST[rekening_kredit_mengangsur]',
        '$_POST[rekening_debet_jasa]',
        '$_POST[rekening_kredit_jasa]',
        '$_POST[nilai_operasional]',
        '$_POST[rekening_debet_provisi]',
        '$_POST[rekening_kredit_provisi]',
        '$_POST[nilai_provisi]',
        '$_POST[rekening_debet_pencairan]',
        '$_POST[rekening_kredit_pencairan]',
        '$_POST[rekening_debet_denda]',
        '$_POST[rekening_kredit_denda]',
        '$_POST[nilai_denda]',
        '$_POST[rekening_debet_admin]',
        '$_POST[rekening_kredit_admin]',
        '$_POST[nilai_admin]')");

    $id_produk = mysql_insert_id();
    $ins = mysql_query("INSERT INTO tb_produksimpanan_histori(id_produk,jasa,biaya_admin,tanggal_berlaku) VALUES ('$id_produk','$_POST[jasa]','$_POST[biaya_admin]',CURDATE())");

    if ($ins) {
        echo "<script>
    document.location=\"?page=pinjaman\"
    </script>";
    } else {
        echo "<script>
    alert(\"Gagal\")
    document.location=\"?page=pinjaman\"
    </script>";
    }
}

switch ($_GET['act']) {
    case 'ins':
        $rek = "SELECT * FROM v_rekening";
?>
        <div class="container-fluid">
            <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Detail Produk</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode_produk">Kode Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="kode_produk" id="kode_produk" placeholder="Kode Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Nama Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="nama_produk" id="nama_produk" placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label for="max_pinjaman">Max Pinjaman</label>
                                    <input type="text" class="form-control form-control-sm" name="max_pinjaman" id="max_pinjaman" placeholder="Max Pinjaman">
                                </div>
                                <div class="form-group">
                                    <label for="jenis_jasa">Jenis Jasa</label>
                                    <div class="input-group">
                                        <select name="jenis_jasa" id="jenis_jasa" class="form-control select2">
                                            <option value="">--Pilih Jenis Jasa--</option>
                                            <option value="F">Flat</option>
                                            <option value="M">Menurun</option>
                                        </select>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <div class="form-check">
                                                    <input type="hidden" name="efektif" value="N">
                                                    <input class="form-check-input" name="efektif" type="checkbox" value="Y">
                                                    <label class="form-check-label">Efektif</label>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jasa_produk">Jasa Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="jasa_produk" id="jasa_produk" placeholder="Jasa Produk">
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_cicilan">Jumlah Cicilan</label>
                                    <input type="text" class="form-control form-control-sm" name="jumlah_cicilan" id="jumlah_cicilan" placeholder="Jumlah Cicilan">
                                </div>
                                <div class="form-group">
                                    <label for="hitung_jasa">Metode Hitung Jasa</label>
                                    <select name="hitung_jasa" id="hitung_jasa" class="form-control form-control-sm">
                                        <option value="">--Pilih--</option>
                                        <?php
                                        $q_hit = mysql_query("SELECT * FROM tb_metodehitungjasa");
                                        while ($r_hit = mysql_fetch_array($q_hit)) {
                                            echo "<option value='$r_hit[id]'>$r_hit[nama]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Detail Rekening</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="rekening_debet_pencairan">Rekening Debet Pencairan</label>
                                    <select name="rekening_debet_pencairan" id="rekening_debet_pencairan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_pencairan">Rekening Kredit Pencairan</label>
                                    <select name="rekening_kredit_pencairan" id="rekening_kredit_pencairan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_operasional">Rekening Debet Operasional</label>
                                    <select name="rekening_debet_operasional" id="rekening_debet_operasional" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_operasional">Rekening Kredit Operasional</label>
                                    <select name="rekening_kredit_operasional" id="rekening_kredit_operasional" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nilai_operasional">Nilai Operasional</label>
                                    <div class="input-group">
                                        <input type="text" name="nilai_operasional" class="form-control form-control-sm" placeholder="Nilai Operasional">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>%</b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_provisi">Rekening Debet Provisi</label>
                                    <select name="rekening_debet_provisi" id="rekening_debet_provisi" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_provisi">Rekening Kredit Provisi</label>
                                    <select name="rekening_kredit_provisi" id="rekening_kredit_provisi" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nilai_provisi">Nilai Provisi</label>
                                    <div class="input-group">
                                        <input type="text" name="nilai_provisi" class="form-control form-control-sm" placeholder="Nilai Provisi">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>%</b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_denda">Rekening Debet Denda</label>
                                    <select name="rekening_debet_denda" id="rekening_debet_denda" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_denda">Rekening Kredit Denda</label>
                                    <select name="rekening_kredit_denda" id="rekening_kredit_denda" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nilai_denda">Nilai Denda</label>
                                    <div class="input-group">
                                        <input type="text" name="nilai_denda" class="form-control form-control-sm" placeholder="Nilai Provisi">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>%</b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_admin">Rekening Debet Administrasi</label>
                                    <select name="rekening_debet_admin" id="rekening_debet_admin" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_admin">Rekening Kredit Administrasi</label>
                                    <select name="rekening_kredit_admin" id="rekening_kredit_admin" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nilai_admin">Nilai Administrasi</label>
                                    <div class="input-group">
                                        <input type="text" name="nilai_admin" class="form-control form-control-sm" placeholder="Nilai Administrasi">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>%</b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_mengangsur">Rekening Debet Mengangsur</label>
                                    <select name="rekening_debet_mengangsur" id="rekening_debet_mengangsur" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_mengangsur">Rekening Kredit Mengangsur</label>
                                    <select name="rekening_kredit_mengangsur" id="rekening_kredit_mengangsur" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="rekening_debet_jasa">Rekening Debet Jasa</label>
                                    <select name="rekening_debet_jasa" id="rekening_debet_jasa" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_jasa">Rekening Kredit Jasa</label>
                                    <select name="rekening_kredit_jasa" id="rekening_kredit_jasa" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script>
            // $('#rekening_debet_setoran, #rekening_kredit_setoran, #rekening_debet_penarikan, #rekening_kredit_penarikan').select2();
        </script>
    <?php
        break;

    default:
    ?>
        <div class="container-fluid">
            <div class="col-12">
                <div class="card">
                    <div class="card-header ui-sortable-handle">
                        <h3 class="card-title">Daftar Produk Pinjaman</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Minimize data">
                                <i class="fas fa-minus"></i>
                            </button>
                            <a href="?page=pinjaman&act=ins" class="btn btn-tool" title="Tambah Simpanan Modal">
                                <i class="fas fa-plus"></i>
                            </a>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Tutup Data">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">No.</th>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Nama Produk</th>
                                    <th class="text-center">Jasa</th>
                                    <th class="text-center"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $q_simo = mysql_query("SELECT * FROM tb_produkpinjaman");
                                while ($r_simo = mysql_fetch_array($q_simo)) {
                                ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $r_simo['kode_produk']; ?></td>
                                        <td><?= $r_simo['nama_produk']; ?></td>
                                        <td class="text-right"><?= ($r_simo['jasa'] * 100) . "%" ?></td>
                                        <td class="text-center"><a href="?page=pinjaman&act=edt&id=<?= sha1($r_simo['id_produk']) ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        </script>
<?php
        break;
}
