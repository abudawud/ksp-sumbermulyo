<?php
// require('../../plugins/fpdf/html_table.php');
require('../../plugins/fpdf/fpdf.php');

$pdf = new FPDF('L', 'mm', [148, 210]);
$pdf->SetMargins(8, 12, 8);
$pdf->SetAutoPageBreak(true);

// Slip ADM pinjaman
$pdf->AddPage();

$pdf->Image('../../dist/img/icon/koperasi-logo.png', 15.5, 13.5, 22, 22);

$pdf->Cell(192, 3, '', 'LTR', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(30, 6, '', 'L', 0, 'L');
$pdf->Cell(81, 6, 'KOPERASI SUMBER MULYO', 0, 0, 'C');
$pdf->SetFont('Arial', 'BU', 12);
$pdf->Cell(81, 6, 'PENARIKAN', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, 'BH. NO518/007.BH/XVI.7/410/2013', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, '           No. Bukti Trans. : TK0000', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, '28 Oktober 2013', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, '', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, 'TEGALSARI - AMBULU - JEMBER', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, '', 'R', 0, 'C');
$pdf->Ln();
$pdf->Cell(192, 4, '', 'LBR', 0, 'C');
$pdf->Ln();

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(110, 8, '  TELAH DITARIK TABUNGAN DARI', 'LT', 0, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(82, 8, 'No Rekening : TK0000', 'TR', 0, 'L');
$pdf->SetFont('Arial', '', 10);
// $pdf->Cell(58, 8, date('d/m/Y') . '  s/d  ' . date('d/m/Y'), 'TR', 0, 'C');
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'No. Anggt/Cln', 0, 0, 'L');
$pdf->Cell(70, 7, ': ', 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(76, 7, 'Tanggal : ' . date('d-F-Y'), 0, 0, 'L');
// $pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
// $pdf->Cell(28, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(6, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Nama', 0, 0, 'L');
$pdf->Cell(70, 7, ': ', 0, 0, 'L');
$pdf->Cell(40, 7, 'Saldo lalu', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(28, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(6, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Alamat', 0, 0, 'L');
$pdf->Cell(70, 7, ': ', 0, 0, 'L');
$pdf->Cell(40, 7, 'Setor Tabungan', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(28, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(6, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(110, 7, '', 'L', 0, 'L');
$pdf->Cell(40, 7, 'Saldo Sekarang', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(28, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(6, 7, '', 'R', 0, 'L');
$pdf->Ln();

// $pdf->Cell(192, 6, 'x', 'LTBR', 0, 'L');
// $pdf->Ln();
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(192, 8, '  JENIS PENARIKAN', 'LR', 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(20, 7, 'Tabungan', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(50, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(90, 7, 'Terbilang : ', 'LTR', 0, 'L');
$pdf->Cell(4, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(48, 7, '', 0, 0, 'L');
$pdf->Cell(8, 7, '', 0, 0, 'L');
$pdf->Cell(22, 7, '', 0, 0, 'R');
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(90, 7, '', 'LBR', 0, 'L');
$pdf->Cell(4, 7, '', 'R', 0, 'L');
$pdf->Ln();

$pdf->SetFont('Arial', '', 10);
$pdf->Cell(192, 7, '', 'LR', 0, 'R');

$pdf->Ln(5);
$pdf->Cell(98, 7, '', 0, 0, 'L');
$pdf->Cell(31, 7, 'Petugas', 0, 0, 'C');
$pdf->Cell(31, 7, 'Kasir', 0, 0, 'C');
$pdf->Cell(32, 7, 'Penerima', 0, 0, 'C');
$pdf->Ln(2);

$pdf->SetLineWidth(0.8);
$pdf->Line(19, 101.5, 97, 101.5);
$pdf->Ln(0);

$pdf->SetLineWidth(0.8);
$pdf->Line(19, 110.5, 97, 110.5);
$pdf->Ln(0);

$pdf->SetLineWidth(0.2);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(10, 10, '', 'L', 0, 'L');
$pdf->Cell(30, 10, 'JUMLAH', 0, 0, 'L');
$pdf->Cell(8, 10, 'Rp', 0, 0, 'L');
$pdf->Cell(40, 10, 'xxx', 0, 0, 'R');
$pdf->Cell(104, 10, '', 'R', 0, 'R');
$pdf->Ln();

$pdf->SetFont('Arial', '', 9);
$pdf->Cell(100, 7, '   Struk dianggap syah jika sudah dilegalisir oleh kasir', 'L', 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(26, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 0, 0, 'C');
$pdf->Cell(26, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 0, 0, 'C');
$pdf->Cell(25, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', 'I', 8);
$pdf->Cell(140, 6, '', 'LB', 0, 'C');
$pdf->Cell(52, 6, date('d/m/y H:i:s'), 'RB', 0, 'C');
$pdf->Ln();


$pdf->Output();
