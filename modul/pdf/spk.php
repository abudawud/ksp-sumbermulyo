<?php
require('../../db.php');
// require('../../plugins/fpdf/html_table.php');
require('../../plugins/fpdf/fpdf.php');

function penyebut($nilai)
{
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
        $temp = penyebut($nilai - 10) . " belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
}

function terbilang($nilai)
{
    if ($nilai < 0) {
        $hasil = "minus " . trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}

$data_pinjaman = mysql_fetch_array(mysql_query("SELECT *, a.alamat as alamat_peminjam,
case
when date_format(tgl_pembukaan,'%w') = 0 THEN 'Minggu'
when date_format(tgl_pembukaan,'%w') = 1 THEN 'Senin'
when date_format(tgl_pembukaan,'%w') = 2 THEN 'Selasa'
when date_format(tgl_pembukaan,'%w') = 3 THEN 'Rabu'
when date_format(tgl_pembukaan,'%w') = 4 THEN 'Kamis'
when date_format(tgl_pembukaan,'%w') = 5 THEN 'Jumat'
when date_format(tgl_pembukaan,'%w') = 6 THEN 'Sabtu'
end as hari,
date_format(tgl_pembukaan,'%d') as tanggal_pembukaan, 
CASE DATE_FORMAT(tgl_pembukaan,'%m') 
    WHEN 1 THEN 'Januari' 
    WHEN 2 THEN 'Februari' 
    WHEN 3 THEN 'Maret' 
    WHEN 4 THEN 'April' 
    WHEN 5 THEN 'Mei' 
    WHEN 6 THEN 'Juni' 
    WHEN 7 THEN 'Juli' 
    WHEN 8 THEN 'Agustus' 
    WHEN 9 THEN 'September'
    WHEN 10 THEN 'Oktober' 
    WHEN 11 THEN 'November' 
    WHEN 12 THEN 'Desember' 
  END as bulan_pembukaan, 
date_format(tgl_pembukaan,'%Y') as tahun_pembukaan, 
date_format(tgl_pembukaan,'%H:%i') as jam_pembukaan 
FROM `tb_rekening` r 
LEFT JOIN tb_anggota a on a.id = r.id_anggota 
LEFT JOIN tb_jaminan_kendaraan k on k.id_rekening = r.id_rekening
where sha1(r.id_rekening) = '$_GET[id]'"));

$pinjaman = number_format($data_pinjaman['jumlah_pinjaman'], 0, '.', ',');

$pdf = new FPDF('P', 'mm', 'A4');
$pdf->SetMargins(10, 10, 10);

// Bukti penerimaan
$pdf->AddPage();

$pdf->Image('../../dist/img/icon/koperasi-logo.png', 10, 10, 25, 25);

$pdf->SetX(38);
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(20, 5, 'KOPERASI SIMPAN PINJAM', 0, 0, 'L');
$pdf->Ln();

$pdf->SetX(38);
$pdf->SetFont('Arial', 'B', 13);
$pdf->Cell(20, 5, 'SUMBER MULYO', 0, 0, 'L');
$pdf->Ln();

$pdf->SetX(38);
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(20, 5, 'BH. NO. 518/007.BH/XVI.7/410/2013', 0, 0, 'L');
$pdf->Ln();

$pdf->SetX(38);
$pdf->Cell(20, 5, 'Tgl 28 Oktober 2013', 0, 0, 'L');
$pdf->Ln();

$pdf->SetX(38);
$pdf->Cell(20, 5, 'Jl. Semeru No. 20 Tegalsari-Ambulu-Jember No Telp 0336.8861012', 0, 0, 'L');
$pdf->Ln(7);

$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(60, 10, 'No : ' . $data_pinjaman['no_rekening'], 1, 0, 'C');
$pdf->Cell(4);
$pdf->Cell(60, 10, 'BUKTI PENERIMAAN', 1, 0, 'C');
$pdf->Cell(4);
$pdf->Cell(60, 10, '', 1, 0, 'C');
$pdf->Ln(14);

$pdf->SetFont('Arial', '', 11);
$pdf->Cell(188, 8, '  Kami telah menerima dengan baik dari :', 'TRL', 0, 'L');
$pdf->Ln();

$pdf->Cell(50, 8, '  Nama', 'L', 0, 'L');
$pdf->Cell(3, 8, ':', 0, 0, 'L');
$pdf->Cell(135, 8, $data_pinjaman['nama_anggota'], 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(50, 8, '  Alamat', 'L', 0, 'L');
$pdf->Cell(3, 8, ':', 0, 0, 'L');
$pdf->Cell(135, 8, $data_pinjaman['alamat'], 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(50, 8, '  No. Anggota', 'L', 0, 'L');
$pdf->Cell(3, 8, ':', 0, 0, 'L');
$pdf->Cell(50, 8, $data_pinjaman['kode'], 0, 0, 'L');
$pdf->Cell(85, 8, 'Avalis  : ', 'R', 0, 'L');
$pdf->Ln();

$pdf->Cell(188, 8, '  Berupa : ', 'TRL', 0, 'L');
$pdf->Ln();

$arr = ['BPKB No', 'Merk', 'Tahun', 'No. Polisi', 'Warna', 'No. Rangka', 'No. Mesin', 'STNK a/n', 'Alamat'];
$isinya1 = [$data_pinjaman['no_bpkb'], $data_pinjaman['merk'], $data_pinjaman['tahun'], $data_pinjaman['no_pol'], $data_pinjaman['warna'], $data_pinjaman['no_rangka'], $data_pinjaman['no_mesin'], $data_pinjaman['nama_pemilik'], $data_pinjaman['alamat']];
$i = 0;
while ($i < count($arr)) {
    $pdf->Cell(30, 8, '', 'L', 0, 'L');
    $pdf->Cell(45, 8, $arr[$i], 0, 0, 'L');
    $pdf->Cell(3, 8, ':', 0, 0, 'L');
    $pdf->Cell(110, 8, $isinya1[$i], 'R', 0, 'L');
    $pdf->Ln();
    $i++;
}

$pdf->Cell(188, 5, '', 'RL', 0, 'L');
$pdf->Ln();

$pdf->Cell(188, 8, '  Besar pinjaman : Rp.' . $pinjaman, 'RL', 0, 'L');
$pdf->Ln();
$pdf->Cell(188, 8, '  Untuk digunakan sebagai jaminan (agunan) sesuai dengan perjanjian kredit', 'RL', 0, 'L');
$pdf->Ln();
$pdf->Cell(90, 8, '  No. ' . $data_pinjaman['no_rekening'], 'L', 0, 'L');
$pdf->Cell(98, 8, '  Jatuh Tempo xxx', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(90, 8, '', 'BL', 0, 'L');
$pdf->Cell(98, 8, '  Bunga tiap bulan Rp. xxx', 'BR', 0, 'L');
$pdf->Ln(12);

$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(60, 10, 'Diperiksa', 'LTR', 0, 'C');
$pdf->Cell(4);
$pdf->Cell(60, 10, 'Diterima', 'LTR', 0, 'C');
$pdf->Cell(4);
$pdf->Cell(60, 10, 'Diserahkan', 'LTR', 0, 'C');
$pdf->Ln();
$pdf->Cell(60, 15, '', 'LR', 0, 'C');
$pdf->Cell(4);
$pdf->Cell(60, 15, '', 'LR', 0, 'C');
$pdf->Cell(4);
$pdf->Cell(60, 15, '', 'LR', 0, 'C');
$pdf->Ln();
$pdf->Cell(60, 10, 'xxx', 'LBR', 0, 'C');
$pdf->Cell(4);
$pdf->Cell(60, 10, '', 'LBR', 0, 'C');
$pdf->Cell(4);
$pdf->Cell(60, 10, 'xxx', 'LBR', 0, 'C');
$pdf->Ln();


// Surat tanda pinjam pakai
$pdf->AddPage();
$pdf->Image('../../dist/img/icon/koperasi-logo.png', 12, 7.5, 23, 23);

$pdf->SetX(38);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(0, 5, 'KOPERASI SIMPAN PINJAM', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->Cell(0, 5, 'SUMBER MULYO', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(0, 5, 'BH. NO518/007.BH/XVI.7/410/2013', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->Cell(0, 5, 'Jalan Semeru No.20 Tegalsari Ambulu Jember', 0, 0, 'C');
$pdf->Ln();
$pdf->SetLineWidth(1);
$pdf->Line(12, 32, 210 - 12, 32);
$pdf->Ln();
$pdf->SetLineWidth(0.5);
$pdf->Line(11.8, 33.2, 210 - 11.8, 33.2);
$pdf->Ln(1.5);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 5, 'SURAT TANDA PINJAM-PAKAI BARANG BUKTI JAMINAN', 0, 0, 'C');
$pdf->Ln(6);

$pdf->SetFont('Arial', '', 11);
$pdf->Cell(0, 4, 'Yang bertanda tangan di bawah ini :', 0, 0, 'L');
$pdf->Ln(5);

$pdf->Cell(60, 5, '1. Nama', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'L');
$pdf->Cell(125, 5, $data_pinjaman['nama'], 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(60, 5, '2. Tempat/Tgl. Lahir', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'L');
$pdf->Cell(125, 5, $data_pinjaman['tgl_lahir'], 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(60, 5, '3. Alamat/tempat tinggal', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'L');
$pdf->Cell(125, 5, $data_pinjaman['alamat_peminjam'], 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(60, 5, '4. Alamat kantor/pekerjaan', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'L');
$pdf->Cell(125, 5, 'xxx', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(60, 5, '5. Avalis', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'L');
$pdf->Cell(125, 5, 'xxx', 0, 0, 'L');
$pdf->Ln(6);

$pdf->Cell(37, 5, 'Benar-benar hari ini,', 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(23, 5, $data_pinjaman['hari'], 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(17, 5, 'Tanggal', 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(13, 5, $data_pinjaman['tanggal_pembukaan'], 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(14, 5, ',Bulan', 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(13, 5, $data_pinjaman['bulan_pembukaan'], 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(14, 5, ',Tahun', 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(17, 5, $data_pinjaman['tahun_pembukaan'], 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(14, 5, 'jam ' . $data_pinjaman['jam_pembukaan'] . ' saya telah', 0, 0, 'L');
$pdf->Ln();

$pdf->Cell(14, 5, 'PINJAM-PAKAI barang-barang berupa :', 0, 0, 'L');
$pdf->Ln(6);

$arr2 = ['Merk', 'Warna', 'Type', 'Tahun Pembuatan', 'Nomor Rangka', 'Nomor Mesin', 'Nomor Polisi', 'Nomor BPKB', 'An. STNK', 'Alamat'];
$isinya2 = [$data_pinjaman['merk'], $data_pinjaman['warna'], $data_pinjaman['jenis'], $data_pinjaman['tahun'], $data_pinjaman['no_rangka'], $data_pinjaman['no_mesin'], $data_pinjaman['no_pol'], $data_pinjaman['no_bpkb'], $data_pinjaman['nama_pemilik'], $data_pinjaman['alamat']];

$i2 = 0;
while ($i2 < count($arr2)) {
    $pdf->Cell(50, 5, $arr2[$i2]);
    $pdf->Cell(3, 5, ':', 0, 0, 'L');
    $pdf->Cell(135, 5, $isinya2[$i2], 0, 0, 'L');
    $pdf->Ln();
    $i2++;
}

$pdf->Ln(1);
$pdf->MultiCell(0, 5, 'Yang sedang saya buat JAMINAN KREDIT pada KOPERASI SUMBER MULYO tersebut dalam SURAT PERJANJIAN KREDIT saya tanggal xxx dan janji pertanggung jawaban saya sebagai berikut :', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(6, 5, 'a.', 0, 0, 'L');
$pdf->MultiCell(0, 5, 'Saya selalu mengingat/sadar, bahwa barang-barang tersebut merupakan barang bukti jaminan pada KOPERASI.', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(6, 5, 'b.', 0, 0, 'L');
$pdf->MultiCell(0, 5, 'Saya telah merawat dengan sebaik-baiknya.', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(6, 5, 'c.', 0, 0, 'L');
$pdf->MultiCell(0, 5, 'Saya sanggup tidak akan merusakkan, merubah dengan warna lain.', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(6, 5, 'd.', 0, 0, 'L');
$pdf->MultiCell(0, 5, 'Saya sanggup tidak akan menjual-belikan, menggadaikan, memindah-tangankan, maupun dengan hal-hal yang lain, yang sifatnya akan menyulitkan.', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(6, 5, 'e.', 0, 0, 'L');
$pdf->MultiCell(0, 5, 'Saya sanggup untuk sewaktu-waktu menyediakan/menyerahkan barang-barang bukti jaminan dimaksud kepada KOPERASI apabila diperlukan.', 0, 'J');
$pdf->Ln(0);
$pdf->MultiCell(0, 5, '     Apabila pernyataan/janji saya pada butir a s/d e tersebut diatas saya alpakan/lalaikan, maka saya sanggup mempertanggung jawabkan secara HUKUM yang berlaku di depan pengadilan.', 0, 'J');
$pdf->MultiCell(0, 5, '     Barang/benda yang dijadikan JAMINAN KREDIT pada Koperasi SUMBER MULYO Tegalsari Ambulu langsung menjadi barang/benda yang padanya dikenakan SITA JAMINAN segera setelah', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(6, 5, 'a.', 0, 0, 'L');
$pdf->MultiCell(0, 5, 'Surat perjanjian Kredit tanggal xxx No. xxx', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(6, 5, 'b.', 0, 0, 'L');
$pdf->MultiCell(0, 5, 'Surat tanda PINJAM PAKAI Barang Bukti JAMINAN di daftar dan dilegalisasi oleh Pengadilan Negeri, untuk dijual kepada umum.', 0, 'J');
$pdf->Ln(0);
$pdf->MultiCell(0, 5, '     Demikian pernyataan/perjanjian yang saya buat di depan pejabat yang berwenang pada KOPERASI SUMBER MULYO Tegalsari Ambulu, dengan tidak ada rasa terpaksa maupun dipaksa oleh siapapun, dan surat PERNYATAAN/PERJANJIAN ini dapat dipergunakan sebagai bukti dan tidak keberatan bila didaftar dan dilegalisasi oleh Pengadilan Negeri.', 0, 'J');
$pdf->Ln(0);

$pdf->SetX(130);
$pdf->Cell(23, 5, 'Dibuat di', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'L');
$pdf->Ln();
$pdf->SetX(130);
$pdf->Cell(23, 5, 'Tanggal', 0, 0, 'L');
$pdf->Cell(3, 5, ': ' . $data_pinjaman['tanggal_pembukaan'] . ' ' . $data_pinjaman['bulan_pembukaan'] . ' ' . $data_pinjaman['tahun_pembukaan'], 0, 0, 'L');
$pdf->Ln();
$pdf->SetX(130);
$pdf->Cell(23, 5, 'Jam', 0, 0, 'L');
$pdf->Cell(3, 5, ': ' . $data_pinjaman['jam_pembukaan'], 0, 0, 'L');
$pdf->Ln();

$pdf->Cell(62, 5, 'Pejabat/Petugas KOPERASI', 0, 0, 'C');
$pdf->Cell(62, 5, 'Avalis', 0, 0, 'C');
$pdf->Cell(63, 5, 'Yang Pinjam-Pakai', 0, 0, 'C');
$pdf->Ln(16.2);
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(62, 5, '(xx)', 0, 0, 'C');
$pdf->Cell(62, 5, '( )', 0, 0, 'C');
$pdf->Cell(63, 5, $data_pinjaman['nama_anggota'], 0, 0, 'C');
$pdf->Ln();

// Surat kuasa pengambilan kendaraan
$pdf->AddPage();
$pdf->SetFont('Arial', 'U', 15);
$pdf->Cell(188, 5, 'SURAT KUASA PENGAMBILAN KENDARAAN', 0, 0, 'C');
$pdf->Ln(12);

$pdf->SetFont('Arial', '', 11);
$pdf->Cell(188, 5, 'Yang bertanda tangan dibawah ini :', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(30, 7, 'Nama', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, $data_pinjaman['nama_anggota'], 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(30, 7, 'Pekerjaan', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(30, 7, 'Alamat', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, $data_pinjaman['alamat_peminjam'], 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(65, 7, 'No. Anggota / Anggota Kelompok', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, $data_pinjaman['kode'], 0, 0, 'L');
$pdf->Ln(10);

$pdf->MultiCell(0, 5, 'Dengan ini memberi KUASA PENUH dan tidak dapat ditarik kembali serta tidak akan berakhir karena sebab-sebab apapun kepada :', 0, 'J');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(30, 7, 'Nama', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(30, 7, 'Pekerjaan', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(30, 7, 'Alamat', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln();

$pdf->SetFont('Arial', 'BU', 13);
$pdf->Cell(188, 7, 'KHUSUS', 0, 0, 'C');
$pdf->Ln(10);

$pdf->SetFont('Arial', '', 11);
$pdf->MultiCell(0, 5, 'Untuk dan atas nama dan atau mewakili Pemberi Kuasa untuk mengambil kendaraan yang telah pemberi kuasa jaminkan kepada KSP SUMBER MULYO Jl. Semeru o. 20 Tegalsari-Ambulu, Badan Hukum No.518/007.BH/XVI.7/410/2013 tanggal 28 Oktober 2013, Sesuai dengan Akta Pengakuan Hutang No. xxx tertanggal xxx dengan data-data sebagai berikut :', 0, 'J');
$pdf->Ln(3);

$arr2 = ['Merk', 'Warna', 'Type', 'Tahun Pembuatan', 'Nomor Rangka', 'Nomor Mesin', 'Nomor Polisi', 'Nomor BPKB', 'An. STNK', 'Alamat'];
$isinya2 = [$data_pinjaman['merk'], $data_pinjaman['warna'], $data_pinjaman['jenis'], $data_pinjaman['tahun'], $data_pinjaman['no_rangka'], $data_pinjaman['no_mesin'], $data_pinjaman['no_pol'], $data_pinjaman['no_bpkb'], $data_pinjaman['nama_pemilik'], $data_pinjaman['alamat']];
$i2 = 0;
while ($i2 < count($arr2)) {
    $pdf->Cell(50, 5, $arr2[$i2]);
    $pdf->Cell(3, 5, ':', 0, 0, 'L');
    $pdf->Cell(135, 5, $isinya2[$i2], 0, 0, 'L');
    $pdf->Ln(6);
    $i2++;
}

$pdf->Ln(3);
$pdf->MultiCell(0, 5, 'Dari tangan siapa saja yang memegang kendaraan tersebut, untuk itu segala akibat dan resiko yang timbul dari terbitnya Kuasa ini secara penuh menjadi tanggung jawab Pemberi Kuasa.', 0, 'J');
$pdf->MultiCell(0, 5, 'Demikian kuasa ini dibuat dengan hak untuk dapat dipindahkan kepada pihak lain dan dapat digunakan bilamana terjadinya Wanprestasi.', 0, 'J');
$pdf->Ln();

$pdf->SetX(140);
$pdf->Cell(0, 5, 'Ambulu, ' . $data_pinjaman['tanggal_pembukaan'] . ' ' . $data_pinjaman['bulan_pembukaan'] . ' ' . $data_pinjaman['tahun_pembukaan'], 0, 0, 'L');
$pdf->Ln(8);
$pdf->Cell(62, 5, 'Yang Menerima Kuasa', 0, 0, 'C');
$pdf->Cell(62, 5, 'Avalis', 0, 0, 'C');
$pdf->Cell(63, 5, 'Yang Memberi Kuasa', 0, 0, 'C');
$pdf->Ln(25);
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(62, 5, '(xx)', 0, 0, 'C');
$pdf->Cell(62, 5, '( )', 0, 0, 'C');
$pdf->Cell(63, 5, $data_pinjaman['nama_anggota'], 0, 0, 'C');
$pdf->Ln();

// Surat tanda pinjam pakai
$pdf->AddPage();
$pdf->Image('../../dist/img/icon/koperasi-logo.png', 12, 7.5, 23, 23);

$pdf->SetX(38);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(0, 5, 'KOPERASI SIMPAN PINJAM', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->Cell(0, 5, 'SUMBER MULYO', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(0, 5, 'BH. NO518/007.BH/XVI.7/410/2013', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->Cell(0, 5, 'Jalan Semeru No.20 Tegalsari Ambulu Jember', 0, 0, 'C');
$pdf->Ln();
$pdf->SetLineWidth(1);
$pdf->Line(12, 32, 210 - 12, 32);
$pdf->Ln();
$pdf->SetLineWidth(0.5);
$pdf->Line(11.8, 33.2, 210 - 11.8, 33.2);
$pdf->Ln(7);

$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(188, 5, 'SURAT TANDA TERIMA PINJAM PAKAI BARANG JAMINAN', 0, 0, 'C');
$pdf->Ln(8);

$pdf->SetFont('Arial', '', 11);
$pdf->Cell(188, 5, 'Yang bertanda tangan dibawah ini :', 0, 0, 'L');
$pdf->Ln(6);
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(45, 7, 'Nama', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(45, 7, 'Tempat/Tgl. Lahir', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(45, 7, 'Pekerjaan', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(45, 7, 'Alamat', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->Cell(65, 7, 'No. Anggota / Anggota Kelompok', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'L');
$pdf->Cell(50, 7, '', 0, 0, 'L');
$pdf->Ln(12);

$pdf->MultiCell(0, 5, 'Telah memberikan barang jaminan milik saya kepada KOPERASI SUMBER MULYO sebagai persyaratan pengajuan kredit xxx', 0, 'J');
$pdf->Ln();

$pdf->Cell(65, 7, 'Adapun barang jaminan berupa :', 0, 0, 'L');
$pdf->Ln(10);

$arr2 = ['Merk', 'Warna', 'Type', 'Tahun Pembuatan', 'Nomor Rangka', 'Nomor Mesin', 'Nomor Polisi', 'Nomor BPKB', 'An. STNK', 'Alamat'];
$isinya2 = [$data_pinjaman['merk'], $data_pinjaman['warna'], $data_pinjaman['jenis'], $data_pinjaman['tahun'], $data_pinjaman['no_rangka'], $data_pinjaman['no_mesin'], $data_pinjaman['no_pol'], $data_pinjaman['no_bpkb'], $data_pinjaman['nama_pemilik'], $data_pinjaman['alamat']];
$isinya3 = [];
$i2 = 0;
while ($i2 < count($arr2)) {
    $pdf->Cell(50, 5, $arr2[$i2]);
    $pdf->Cell(3, 5, ':', 0, 0, 'L');
    $pdf->Cell(135, 5, $isinya2[$i2], 0, 0, 'L');
    $pdf->Ln(6);
    $i2++;
}

$pdf->Ln(6);
$pdf->Cell(188, 5, 'Demikian surat tanda terima penyerahan barang jaminan ini dibuat untuk digunakan seperlunya.', 0, 0, 'L');
$pdf->Ln(17);
$pdf->SetX(140);
$pdf->Cell(50, 5, 'Ambulu, ' . $data_pinjaman['tanggal_pembukaan'] . ' ' . $data_pinjaman['bulan_pembukaan'] . ' ' . $data_pinjaman['tahun_pembukaan'], 0, 0, 'L');
$pdf->Ln(8);
$pdf->Cell(62, 5, 'Yang Menerima Kuasa', 0, 0, 'C');
$pdf->Cell(62, 5, 'Avalis', 0, 0, 'C');
$pdf->Cell(63, 5, 'Yang Memberi Kuasa', 0, 0, 'C');
$pdf->Ln(25);
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(62, 5, '(xx)', 0, 0, 'C');
$pdf->Cell(62, 5, '( )', 0, 0, 'C');
$pdf->Cell(63, 5, $data_pinjaman['nama_anggota'], 0, 0, 'C');
$pdf->Ln();

// Surat perjanjian kredit
$pdf->AddPage();
$pdf->Image('../../dist/img/icon/koperasi-logo.png', 12, 7.5, 23, 23);

$pdf->SetX(38);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(100, 5, 'KOPERASI', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->Cell(100, 5, 'SUMBER MULYO', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(100, 5, 'BH. NO518/007.BH/XVI.7/410/2013', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->Cell(100, 5, 'Jalan Semeru No.20 Tegalsari Ambulu Jember', 0, 0, 'C');
$pdf->Ln(14);

$pdf->SetFont('Arial', 'BU', 18);
$pdf->Cell(188, 5, 'SURAT PERJANJIAN KREDIT', 0, 0, 'C');
$pdf->Ln(7);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(188, 5, 'NOMOR : xxx', 0, 0, 'C');
$pdf->Ln(8);

$pdf->SetFont('Arial', '', 11);
$pdf->Cell(188, 5, 'Yang bertanda tangan dibawah ini', 0, 0, 'L');
$pdf->Ln();

$pdf->Cell(5, 5, 'I.', 0, 0, 'L');
$pdf->MultiCell(0, 5, '..................................... Pimpinan dari dan oleh karena itu bertindak untuk dan atas nama KOPERASI SUMBER MULYO selanjutnya disebut KOPERASI.', 0, 'J');
$pdf->Ln(0);

$pdf->Cell(5, 6, 'II.', 0, 0, 'L');
$pdf->Cell(30, 6, 'Nama', 0, 0, 'L');
$pdf->Cell(3, 6, ':', 0, 0, 'L');
$pdf->Cell(100, 6, $data_pinjaman['nama_anggota'], 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(5, 6, '', 0, 0, 'L');
$pdf->Cell(30, 6, 'Alamat', 0, 0, 'L');
$pdf->Cell(3, 6, ':', 0, 0, 'L');
$pdf->Cell(100, 6, $data_pinjaman['alamat_peminjam'], 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(5, 6, '', 0, 0, 'L');
$pdf->Cell(30, 6, 'Pekerjaan', 0, 0, 'L');
$pdf->Cell(3, 6, ':', 0, 0, 'L');
$pdf->Cell(100, 6, '', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(5, 6, '', 0, 0, 'L');
$pdf->Cell(130, 6, 'Selanjutnya disebut PEMINJAM', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(130, 6, 'Menyatakan bersama-sama mengadakan perjanjian dengan ketentuan dan syarat-syarat sebagai berikut :', 0, 0, 'L');
$pdf->Ln(10);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 1', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->MultiCell(0, 5, 'Peminjam mengakui telah menerima uang sebagai pinjaman dari KOPERASI sebagaimana oleh KOPERASI telah diserahkan kepadanya sebagai peminjam uang sebesar Rp xxx () dan untuk penerimaan uang sejumlah itu surat perjanjian ini sekaligus berlaku pula sebagai kwitansinya.', 0, 'J');
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 2', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(98, 5, 'Atas pinjaman uang tersbut, peminjam dikenakan bunga', 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(10, 5, '3,5%', 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(70, 5, 'sebulan dan pinjaman tersebut harus dilunasi.', 0, 0, 'L');
// $pdf->MultiCell(0, 5, 'Atas pinjaman uang tersebut, peminjam dikenakan bunga 3,5% sebulan dan pinjaman tersebut harus dilunasi.', 0, 'J');
// $pdf->MultiCell(0, 5, 'Untuk tiap-tiap hari keterlambatan peminjam melunasi pembayarannya dikenakan denda 1% (sehari) dari jumlah yang terhutang.', 0, 'J');
$pdf->Ln();
$pdf->Cell(145, 5, 'Untuk tiap-tiap hari keterlambatan  peminjam  melunasi pembayarannya  dikenakan', 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(6, 5, '1%', 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(30, 5, ' (sehari)  dari  jumlah', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(30, 5, 'yang terhutang.', 0, 0, 'L');
$pdf->Ln();

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 3', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->MultiCell(0, 5, 'Pinjaman ini dapat ditagih dengan seketika dan sekali lunas sebelum waaktunya, jika peminjam meninggal dunia, jatuh pailit, ditaruh dibawah pengampunan, kehilangan hak untuk mengurus sendiri harta bendanya atau apabila harta benda peminjaman disita/dibeslah, peminjam mengajukan penundaan pembayaran.', 0, 'J');
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 4', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->MultiCell(0, 5, 'Semua ongkos yang tersebut dalam perjanjian ini termasuk ongkos penafsiran, penyimpanan pemeliharaan, pemeriksaan barang-barang jaminan, ongkos pengacara ongkos penjualan dan/atau eksekusi, pendek kata segala macam ongkos yang ditimbulkan karena perjanjian ini, dibebankan dan dipikul oleh peminjam.', 0, 'J');
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 5', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->MultiCell(0, 5, 'Peminjam menyatakan dengan ini menerima baik dan tunduk pada segenap ketentuan-ketentuan yang termasuk dalam perjanjian ini dan segenap peraturan-peraturan serta kebiasaan-kebiasaan KOPERASI perihal kredit.', 0, 'J');
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 6', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->MultiCell(0, 5, 'Peminjam oleh karena kredit yang diterimanya bersedia menjaminkan barang-barang beserta surat-suratnya berupa :', 0, 'J');
$pdf->Ln(1);

$arr2 = ['Merk', 'Warna', 'Type', 'Tahun Pembuatan', 'Nomor Rangka', 'Nomor Mesin', 'Nomor Polisi', 'Nomor BPKB', 'An. STNK', 'Alamat'];
$isinya2 = [$data_pinjaman['merk'], $data_pinjaman['warna'], $data_pinjaman['jenis'], $data_pinjaman['tahun'], $data_pinjaman['no_rangka'], $data_pinjaman['no_mesin'], $data_pinjaman['no_pol'], $data_pinjaman['no_bpkb'], $data_pinjaman['nama_pemilik'], $data_pinjaman['alamat']];
$i2 = 0;
while ($i2 < count($arr2)) {
    $pdf->Cell(50, 5, $arr2[$i2]);
    $pdf->Cell(3, 5, ':', 0, 0, 'L');
    $pdf->Cell(135, 5, $isinya2[$i2], 0, 0, 'L');
    $pdf->Ln(6);
    $i2++;
}

$pdf->Cell(0, 6, 'Dan surat perjanjian ini dianggap sebagai tanda terimanya.', 0, 0, 'L');
$pdf->Ln(10);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 7', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->MultiCell(0, 5, 'Peminjam dengan ini menyatakan bahwa barang-barang jaminan tersebut bebas dari segala beban dan selama pinjaman ini belum lunas tidak akan diberatkan beban apapun kepada pihak ketiga.', 0, 'J');
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 8', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(7, 5, '1.', 0, 0, 'C');
$pdf->MultiCell(0, 5, 'Koperasi selalu berhak untuk sewaktu-waktu menarik kredit yang termasuk dalam perjanjian ini. Apabila pembayaran kembali dari pinjaman tersebut beserta bunga dari provisi yang terhutang oleh peminjam tidak dilakukan sebagaimana mestinya atau peminjam tidak memenuhi kewajibannya terhadap KOPERASI, yang timbul karena perjanjian ini, dan karena KOPERASI memutuskan untuk menarik kembali kredit yang telah diberikan itu, maka tanpa memerlukan teguran lagi, bersama ini peminjam memberi kuasa penuh dengan Hak Subtitusi kepada KOPERASI, kuasa mana tidak dapat ditarik kembali, dibatalkan atau menjadi berhenti dengan melepaskan segala peraturan hukum yang menentukan sebab-sebab karena pada umumnya kuasa-kuasa menjadi berhenti atau batal, khusus untuk menjual barang-barang jaminan diatas dengan cara dan dengan harga yang dianggap baik oleh KOPERASI baik secara lelang dimuka umum maupun secara jual dibawah tangan atau menyerahkan barang-barang jaminan tadi kepada orang lain atas dasar komisi untuk dijual berangsur-angsur, mengambil dan memiliki hasil dari penjualan tersebut dan memperhitungkannya sebagai pembayaran kembali dari hutang peminjam.', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(7, 5, '2.', 0, 0, 'C');
$pdf->MultiCell(0, 5, 'Dalam menentukan ketentuan-ketentuan tersebut diatas, KOPERASI mempunyai hak untuk menentukan sendiri jumlah uang yang harus dibayar oleh peminjam kepada KOPERASI oleh karena pokok pinjaman bunga, provisi dan ongkos-ongkos lainnya.', 0, 'J');
$pdf->Ln(0);
$pdf->Cell(7, 5, '3.', 0, 0, 'C');
$pdf->MultiCell(0, 5, 'Jika kemudian ternyata bahwa jumlah yang ditetapkan oleh KOPERASI itu melebihi jumlah yang sebenarnya yang harus dibayar kepadanya oleh peminjam, peminjam berhak meminta kembali kelebihan itu. Akan tetapi KOPERASI tidak berkewajiban dan karenanya dibebaskan dari membayar suatu kerugian.', 0, 'J');
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PASAL 9', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->MultiCell(0, 5, 'Mengenai perjanjian ini dan segala akibatnya, para pihak memilih domisili dikantor panitera Pengadilan Negeri di Jember.', 0, 'J');
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, 6, 'PENUTUP', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(0, 6, 'Surat perjanjian kredit ini mulai berlaku sejak tanggal ditandatanganinya.', 0, 0, 'L');
$pdf->Ln(6);

$pdf->Cell(94, 6, 'PENERIMA KREDIT', 0, 0, 'C');
$pdf->Cell(94, 6, 'KOPERASI SIMPAN PINJAM', 0, 0, 'C');
$pdf->Ln();

$pdf->Cell(94, 6, '', 0, 0, 'C');
$pdf->Cell(94, 6, 'SUMBER MULYO xxx', 0, 0, 'C');
$pdf->Ln(14);

$pdf->Cell(94, 6, '', 0, 0, 'C');
$pdf->Cell(94, 6, '', 0, 0, 'C');
$pdf->Ln(8);

$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(94, 6, '()', 0, 0, 'C');
$pdf->Cell(94, 6, '()', 0, 0, 'C');
$pdf->Ln();

$pdf->Cell(94, 6, '', 0, 0, 'C');
$pdf->Cell(94, 6, 'Pimpinan', 0, 0, 'C');
$pdf->Ln(15);

$pdf->SetFont('Arial', '', 11);
$pdf->SetLineWidth(0.2);
$pdf->SetX(20);
$pdf->Cell(7, 5, ' 1.', 'LT', 0, 'L');
$pdf->Cell(20, 5, 'Jaminan', 'T', 0, 'L');
$pdf->Cell(3, 5, ':', 'T', 0, 'C');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(140, 5, '', 'RT', 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Ln();

$pdf->SetX(20);
$pdf->Cell(7, 5, ' 2.', 'L', 0, 'L');
$pdf->Cell(163, 5, 'Ketentuan Tambahan', 'R', 0, 'L');
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(7, 5, '', 'L', 0, 'L');
$pdf->Cell(116, 5, 'Para pihak bersepakat untuk menambah perjanjian kredit Nomor : ', 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(20, 5, 'KR14166', 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(27, 5, 'dengan', 'R', 0, 'L');
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(7, 5, '', 'L', 0, 'L');
$pdf->Cell(163, 5, 'ketentuan tambahan.', 'R', 0, 'L');
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(7, 15, '', 'L', 0, 'L');
$pdf->MultiCell(163, 5, 'Bila dalam waktu 10 hari setelah perjanjian kredit jatuh tempo, tidak ada penyelesaian pinjaman, maka KOPERASI diberi kuasa untuk menjual jaminan dengan harga yang dianggap baik oleh KOPERASI, yaitu :', 'R', 'J');
$pdf->Ln(0);
$pdf->SetX(20);
$pdf->Cell(7, 10, '', 'L', 0, 'L');
$pdf->MultiCell(163, 5, 'Rp' . $pinjaman . '(' . terbilang($data_pinjaman['jumlah_pinjaman']) . ' rupiah)', 'R', 'J');
$pdf->Ln(0);

$pdf->SetX(20);
$pdf->Cell(100, 5, '', 'L', 0, 'L');
$pdf->Cell(70, 5, 'Ambulu, ' . $data_pinjaman['tanggal_pembukaan'] . ' ' . $data_pinjaman['bulan_pembukaan'] . ' ' . $data_pinjaman['tahun_pembukaan'], 'R', 0, 'L');
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(85, 5, 'AVALIS', 'L', 0, 'C');
$pdf->Cell(85, 5, 'PEMINJAM', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(85, 13, '', 'L', 0, 'C');
$pdf->Cell(85, 13, '', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetX(20);
$pdf->SetFont('Arial', 'BU', 11);
$pdf->Cell(85, 10, '( )', 'LB', 0, 'C');
$pdf->Cell(85, 10, '(' . $data_pinjaman['nama_anggota'] . ')', 'RB', 0, 'C');
$pdf->Ln(14);

$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(80, 5, 'Barang jaminan telah diterima kembali', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(75, 5, 'Yang Mengambil,', 0, 0, 'C');
$pdf->Ln();
$pdf->Cell(80, 12, '', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', 'U', 11);
$pdf->Cell(75, 5, '(                                              )', 0, 0, 'C');

// Permohonan pinjam pakai barang jaminan
$pdf->AddPage();
$pdf->SetFont('Arial', '', 11);

$pdf->Cell(18, 5, 'Perihal', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'C');
$pdf->MultiCell(65, 5, 'Permohonan pinjam pakai-Barang jaminan kendaraan Roda 2/Roda 4', 0, 'J');
$pdf->Ln(3);

$pdf->SetX(110);
$pdf->Cell(20, 7, 'Kepada', 0, 0, 'L');
$pdf->Ln();
$pdf->SetX(110);
$pdf->Cell(20, 7, 'Yth. Pimpinan Koperasi SUMBER MULYO', 0, 0, 'L');
$pdf->Ln();
$pdf->SetX(110);
$pdf->Cell(20, 7, 'di', 0, 0, 'L');
$pdf->Ln();
$pdf->SetX(110);
$pdf->Cell(20, 7, '           AMBULU', 0, 0, 'L');
$pdf->Ln();

$pdf->Cell(20);
$pdf->Cell(20, 7, 'Yang bertanda tangan dibawah ini saya :', 0, 0, 'L');
$pdf->Ln();

$pdf->Cell(46, 7, 'Nama', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(70, 7, $data_pinjaman['nama_anggota'], 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Ln();
$pdf->Cell(46, 7, 'Tempat / Tgl. Lahir', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(70, 7, $data_pinjaman['tgl_lahir'], 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Ln();
$pdf->Cell(46, 7, 'Pekerjaan', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(70, 7, '', 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Ln();
$pdf->Cell(46, 7, 'Alamat / tempat tinggal', 0, 0, 'L');
$pdf->Cell(3, 7, ':', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(70, 7, $data_pinjaman['alamat_peminjam'], 0, 0, 'L');
$pdf->SetFont('Arial', '', 11);
$pdf->Ln(12);

$pdf->MultiCell(0, 5, 'Pada hari ini ' . $data_pinjaman['hari'] . ' jam ' . $data_pinjaman['jam_pembukaan'] . ' Wib, saya tersebut diatas mengajukan permohonan PINJAM PAKAI barang jaminan berupa.', 0, 'J');
$pdf->Ln(3);

$arr2 = ['Merk', 'Warna', 'Type', 'Tahun Pembuatan', 'Nomor Rangka', 'Nomor Mesin', 'Nomor Polisi', 'Nomor BPKB', 'An. STNK', 'Alamat'];
$isinya2 = [$data_pinjaman['merk'], $data_pinjaman['warna'], $data_pinjaman['jenis'], $data_pinjaman['tahun'], $data_pinjaman['no_rangka'], $data_pinjaman['no_mesin'], $data_pinjaman['no_pol'], $data_pinjaman['no_bpkb'], $data_pinjaman['nama_pemilik'], $data_pinjaman['alamat']];
$i2 = 0;
while ($i2 < count($arr2)) {
    $pdf->Cell(50, 5, $arr2[$i2]);
    $pdf->Cell(3, 5, ':', 0, 0, 'L');
    $pdf->Cell(135, 5, $isinya2[$i2], 0, 0, 'L');
    $pdf->Ln(6);
    $i2++;
}

$pdf->Ln(3);
$pdf->MultiCell(0, 5, 'Yang telah saya serahkan kepada Koperasi SUMBER MULYO sebagai barang jaminan untuk persyaratan permohonan.', 0, 'J');
$pdf->Ln(5);
$pdf->Cell(0, 5, 'Dengan catatan bahwa saya bersedia untuk :', 0, 0, 'L');
$pdf->Ln(7);
$pdf->Cell(6, 5, '1.', 0, 0, 'L');
$pdf->Cell(0, 5, 'Tidak memindah tangankan baik itu menjual, menggadaikan, menyerahkan kepada pihak lain.', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(6, 5, '2.', 0, 0, 'L');
$pdf->Cell(0, 5, 'Tidak merubah warna, sifat maupun bentuknya.', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(6, 5, '3.', 0, 0, 'L');
$pdf->MultiCell(0, 5, 'Sewaktu-waktu barang tersebut dibutuhkan oleh pihak Koperasi SUMBER MULYO maka saya sanggup untuk menyediakan atau menyerahkan.', 0, 'J');
$pdf->Ln(5);

$pdf->MultiCell(0, 5, 'Kemudian apabila ternyata dikemudian hari saya tidak menepati sesuai surat permohonan yang saya tanda tangani ini, maka saya sanggup dituntut berdasarkan hukum yang berlaku.', 0, 'J');
$pdf->Ln(5);

$pdf->MultiCell(0, 5, 'Demikian Surat Permohonan pinjam pakai barang jaminan ini saya buat dengan sebenarnya dan dapat dipergunakan seperlunya.', 0, 'J');
$pdf->Ln(2);

$pdf->SetX(120);
$pdf->Cell(20, 7, 'Ambulu, ' . $data_pinjaman['tanggal_pembukaan'] . ' ' . $data_pinjaman['bulan_pembukaan'] . ' ' . $data_pinjaman['tahun_pembukaan'], 0, 0, 'L');
$pdf->Ln();
$pdf->SetX(120);
$pdf->Cell(50, 7, 'Pemohon', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(120);
$pdf->Cell(50, 14, '', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(120);
$pdf->SetFont('Arial', 'BU', 11);
$pdf->Cell(50, 7, '(' . $data_pinjaman['nama_anggota'] . ')', 0, 0, 'C');


// Permohonan pemblokiran kendaraan bermotor
$pdf->AddPage();
$pdf->Image('../../dist/img/icon/koperasi-logo.png', 12, 7.5, 23, 23);

$pdf->SetX(38);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(0, 5, 'KOPERASI SIMPAN PINJAM', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->Cell(0, 5, 'SUMBER MULYO', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(0, 5, 'BH. NO. 518/007.BH/XV.7/410/2013', 0, 0, 'C');
$pdf->Ln();
$pdf->SetX(38);
$pdf->Cell(0, 5, 'Jalan Semeru No.20 Tegalsari Ambulu Jember', 0, 0, 'C');
$pdf->Ln();
$pdf->SetLineWidth(1);
$pdf->Line(12, 32, 210 - 12, 32);
$pdf->Ln();
$pdf->SetLineWidth(0.5);
$pdf->Line(11.8, 33.2, 210 - 11.8, 33.2);
$pdf->Ln(1.5);

$pdf->SetLineWidth(0.2);
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(30, 5, 'Nomor', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'C');
$pdf->Cell(85, 5, '', 0, 0, 'L');
$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(30, 5, 'Klasifikasi', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'C');
$pdf->Cell(85, 5, '', 0, 0, 'L');
$pdf->Cell(30, 5, 'Yth. KADIT LANTAS POLDA JATIM', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(30, 5, 'Lampiran', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'C');
$pdf->Cell(85, 5, '', 0, 0, 'L');
$pdf->Cell(30, 5, 'di', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(30, 5, 'Perihal', 0, 0, 'L');
$pdf->Cell(3, 5, ':', 0, 0, 'C');
$pdf->Cell(85, 5, 'Permohonan Pemblokiran', 0, 0, 'L');
$pdf->Cell(30, 5, '      SURABAYA', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(30, 5, '', 0, 0, 'L');
$pdf->Cell(3, 5, '', 0, 0, 'C');
$pdf->Cell(85, 5, 'Kendaraan bermotor', 0, 0, 'L');
$pdf->Ln(15);

$pdf->SetFont('Arial', 'BU', 14);
$pdf->Cell(0, 5, 'U.P KABAGMIN REG. INDENT', 0, 0, 'C');
$pdf->Ln(15);

$pdf->SetFont('Arial', '', 11);
$pdf->Cell(30);
$pdf->Cell(0, 5, 'Dengan hormat,', 0, 0, 'L');
$pdf->Ln();
$pdf->MultiCell(0, 5, '                           Sehubungan dengan telah dipergunakannya jaminan kredit pada Koperasi Sumber Mulyo di Ambulu atas kendaraan bermotor dengan data sebagai berikut :', 0, 'J');
$pdf->Ln(3);

$arr2 = ['Merk', 'Warna', 'Type', 'Tahun Pembuatan', 'Nomor Rangka', 'Nomor Mesin', 'Nomor Polisi', 'Nomor BPKB', 'An. STNK', 'Alamat'];
$isinya2 = [$data_pinjaman['merk'], $data_pinjaman['warna'], $data_pinjaman['jenis'], $data_pinjaman['tahun'], $data_pinjaman['no_rangka'], $data_pinjaman['no_mesin'], $data_pinjaman['no_pol'], $data_pinjaman['no_bpkb'], $data_pinjaman['nama_pemilik'], $data_pinjaman['alamat']];
$i2 = 0;
while ($i2 < count($arr2)) {
    $pdf->Cell(50, 5, $arr2[$i2]);
    $pdf->Cell(3, 5, ':', 0, 0, 'L');
    $pdf->Cell(135, 5, $isinya2[$i2], 0, 0, 'L');
    $pdf->Ln(6);
    $i2++;
}

$pdf->Ln(3);
$pdf->MultiCell(0, 5, 'Maka dengan ini kami memohon bantuan saudara untuk tidak melayani permohonan penggantian BPKB atau Penggantian / pengalihan hak pemilikan atas kendaraan tersebut diatas dengan alasan apapun dan oleh siapapun tanpa persetujuan tertulis dari Koperasi.', 0, 'J');
$pdf->Ln(0);
$pdf->MultiCell(0, 5, '                           Jika kemudian hari terjadi pembebasan sebagai jaminan kredit atas kendaraan tersebut oleh pihak kami, maka hal tersebut akan kami laporkan secara tertulis.', 0, 'J');
$pdf->Ln(0);
$pdf->MultiCell(0, 5, '                           Demikian atas bantuan dan kerja sama yang baik, kami menyampaikan terima kasih.', 0, 'J');
$pdf->Ln(10);

$pdf->SetX(120);
$pdf->Cell(20, 7, 'Ambulu, ' . $data_pinjaman['tanggal_pembukaan'] . ' ' . $data_pinjaman['bulan_pembukaan'] . ' ' . $data_pinjaman['tahun_pembukaan'], 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(94, 7, 'Mengetahui', 0, 0, 'C');
$pdf->Cell(94, 7, 'Yang Menyerahkan', 0, 0, 'C');
$pdf->Ln();
$pdf->Cell(94, 20, '', 0, 0, 'C');
$pdf->Cell(94, 20, '', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', 'BU', 11);
$pdf->Cell(94, 5, '( )', 0, 0, 'C');
$pdf->Cell(94, 5, '( )', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(94, 5, 'Yang Menjamin Kendaraan', 0, 0, 'C');
$pdf->Cell(94, 5, 'Pimpinan', 0, 0, 'C');
$pdf->Ln(20);

$pdf->SetFont('Arial', 'BU', 11);
$pdf->Cell(0, 5, 'Tembusan :', 0, 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(0, 5, 'Kasat Lantas Polres Jember', 0, 0, 'L');


$pdf->Output();
